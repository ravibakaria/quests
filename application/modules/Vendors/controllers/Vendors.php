<?php

class Vendors extends MX_Controller
{
    function __construct()
    {
        $this->load->library('form_validation');
        $this->load->model('Vendors_md'); 
        parent::__construct();
        if($this->session->userdata('user') == ''){
            redirect (base_url("Admin"));
        }
    }

public  function index()
    {
       // echo "WELOME TO HERE HOME";
       $vendor = $this->Vendors_md->get_all_vendor_service(); 
       $data = array( 
           'vendor_data' => $vendor 
       ); 
        $this->load->template('vendor_list',$data);
        //$this->load->view('welcome_message');
    }

    public function create() 
    {  
        $roles = $this->Vendors_md->get_all('roles');
        $rolasses = [];
    $data = array( 
        'button' => 'Create' ,
        'action' => base_url('Vendors/add_vendor') , 
        'id' =>  '', 
        'name' =>  '',
        'email' =>  '',
        'phone' => '',
        'pass' => '',
        'roles' => $roles, 
        'status' => '2',
        'rolasses' => $rolasses,  
    ); 
   
    
    $this->load->template('add_vendor',$data);
    } 

    public function add_vendor() 
    { 
   
        $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('role', 'role', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');  
         
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
    if ($this->form_validation->run() == FALSE) 
        { 
        $this->create(); 
        } 
      else 
        { 
        $refercode = mt_rand(100000, 999999);
        $data = array( 
            'name' => $this->input->post('name', TRUE) , 
            'email' => $this->input->post('email', TRUE) , 
            'phone' => $this->input->post('phone', TRUE) ,
            'password' => md5($this->input->post('password', TRUE)) , 
            'status' => $this->input->post('status', TRUE) , 
            'role' => $this->input->post('role', TRUE) , 
            'created_at' => date('Y-m-d H:i:s') , 
            'refercode' => $refercode , 
        ); 
       // print_r($data);die();
       $insert_id = $this->Vendors_md->insert('admin',$data); 
        if(!empty($_POST['access'])) {
            foreach($_POST['access'] as $check) {   
                $data =[
                    'name' => $check,
                    'user_id' => $insert_id,
                ];

                $this->Vendors_md->insert('rolesacce',$data);  
            }
        }
        $this->session->set_flashdata('message', 'Create Record Success'); 
        redirect(base_url('Vendors')); 
        } 
    }

    public function update($id) 
    { 
    $row = $this->Vendors_md->get_by_id('admin',$id);
    $rolasses = $this->Vendors_md->get_by_id_array('rolesacce',$id);
    $roles = $this->Vendors_md->get_all('roles');  
    if ($row) 
        { 
            if($row->isadmin == 1){
                $this->session->set_flashdata('message', 'Cannot Update This Record'); 
                redirect(base_url('Vendors')); 
            }
        $data = array( 
            'button' => 'Update', 
            'action' => base_url('Vendors/update_action') , 
            'id' =>  $row->id, 
            'name' => $row->name, 
            'email' => $row->email, 
            'phone' => $row->phone, 
            'pass' => $row->password, 
            'status' => $row->status,
            'roles' => $roles, 
            'role' => $row->role, 
            'rolasses' => $rolasses, 

        ); 
        $this->load->template('add_vendor', $data); 
        } 
      else 
        { 
        $this->session->set_flashdata('message', 'Record Not Found'); 
        redirect(base_url('Vendors')); 
        } 
    }
    
    public function update_action() 
        { 
            // echo "<pre>";
            // print_r($_POST);die();
            $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
            $this->form_validation->set_rules('email', 'Email', 'trim|required'); 
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
        if ($this->form_validation->run() == FALSE) 
            { 
            $this->update($this->input->post('id', TRUE)); 
            } 
          else  
            { 
            $data = array( 
                'name' => $this->input->post('name', TRUE) , 
                'email' => $this->input->post('email', TRUE) , 
                'phone' => $this->input->post('phone', TRUE) ,
                'status' => $this->input->post('ststus', TRUE) , 
                'role' => $this->input->post('role', TRUE) , 
                // 'created_at' => date('Y-m-d H:i:s') , 
            ); 
            $this->Vendors_md->update('admin',$this->input->post('id', TRUE) , $data);
            if(!empty($_POST['access'])) {
                $this->Vendors_md->deleteasses('rolesacce',$this->input->post('id', TRUE));
                foreach($_POST['access'] as $check) {   
                    $data =[
                        'name' => $check,
                        'user_id' => $this->input->post('id', TRUE)
                    ];
    
                    $this->Vendors_md->insert('rolesacce',$data);  
                } 
            }
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Vendors')); 
            } 
        } 

       public function _rules() 
        { 
        $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('service', 'Service', 'trim|required');  
        $this->form_validation->set_rules('id', 'id', 'trim'); 
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
        }

       public function delete($id) 
        { 
        $row = $this->Vendors_md->get_by_id('admin',$id); 
        if ($row) 
            { 
                if($row->isadmin == 1){
                    $this->session->set_flashdata('message', ' You Cannot Deleted This Record'); 
                    redirect(base_url('Vendors')); 
                }
            $this->Vendors_md->delete('admin',$id); 
            $this->Vendors_md->deleteasses('rolesacce',$id);
            $this->session->set_flashdata('message', 'Delete Record Success'); 
            redirect(base_url('Vendors')); 
            } 
          else 
            { 
            $this->session->set_flashdata('message', 'Record Not Found'); 
            redirect(base_url('Vendors'));  
            } 
        }

        public  function service()
        {
           // echo "WELOME TO HERE HOME";
           $service = $this->Vendors_md->get_all('service'); 
           $data = array( 
               'service_data' => $service 
           ); 
            $this->load->template('service_list',$data);
            //$this->load->view('welcome_message');
        }

        public function create_service() 
        {  
        $business = $this->Vendors_md->get_all('business');
        $data = array( 
            'button' => 'Create' ,
            'action' => base_url('Vendors/add_service') , 
            'id' =>  '', 
            'name' =>  '',
            'business' => $business,
            'bm' =>''
        ); 
        $this->load->template('add_service',$data);
        }

    public function add_service() 
    { 
    $this->_rulese(); 
    if ($this->form_validation->run() == FALSE) 
        { 
        $this->create_service(); 
        } 
      else 
        { 
        $data = array( 
            'name' => $this->input->post('name', TRUE) ,  
            'created_at' => date('Y-m-d H:i:s') , 
        ); 
        $insert_id = $this->Vendors_md->insert('service',$data); 
        
        if(!empty($_POST['bmodules'])) {
            foreach($_POST['bmodules'] as $check) {   
                $data =[
                    'bussines_id' => $check,
                    'sevice_id' => $insert_id,
                ];

                $this->Vendors_md->insert('business_module',$data);  
            }
        }

        $this->session->set_flashdata('message', 'Create Record Success'); 
        redirect(base_url('Vendors/service')); 
        } 
    }

    public function update_service($id) 
    { 
    $row = $this->Vendors_md->get_by_id('service',$id); 
    $business = $this->Vendors_md->get_all('business');
    $bm = $this->Vendors_md->get_all_bm('business_module', $id);
    if ($row) 
        { 
        $data = array( 
            'button' => 'Update', 
            'action' => base_url('Vendors/update_service_action') , 
            'id' =>  $row->id, 
            'name' => $row->name, 
            'business' => $business,
            'bm' =>$bm
        ); 
        // echo "<pre>";
        // print_r($data);die;
        $this->load->template('add_service', $data); 
        } 
      else 
        { 
        $this->session->set_flashdata('message', 'Record Not Found'); 
        redirect(base_url('Vendors/service')); 
        } 
    }

    public function update_service_action() 
        { 
           // print_r($_POST);die;
        $this->_rulese(); 
        if ($this->form_validation->run() == FALSE) 
            { 
            $this->update_service($this->input->post('id', TRUE)); 
            } 
          else 
            { 
            $data = array( 
                'name' => $this->input->post('name', TRUE) , 
                // 'email' => $this->input->post('email', TRUE) , 
                // 'phone' => $this->input->post('phone', TRUE) , 
                // 'created_at' => date('Y-m-d H:i:s') , 
            ); 
            $this->Vendors_md->update_service('service',$this->input->post('id', TRUE) , $data); 
            if(empty($_POST['bmodules'])) {
                $this->Vendors_md->deletebusiness_module('business_module',$this->input->post('id', TRUE));
            }
            if(!empty($_POST['bmodules'])) {
                $this->Vendors_md->deletebusiness_module('business_module',$this->input->post('id', TRUE));
                foreach($_POST['bmodules'] as $check) {   
                    $data =[
                        'bussines_id' => $check,
                        'sevice_id' => $this->input->post('id', TRUE)
                    ];
    
                    $this->Vendors_md->insert('business_module',$data);  
                } 
            }
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Vendors/service')); 
            } 
        }

        public function _rulese() 
        { 
        $this->form_validation->set_rules('name', 'Name', 'trim|required');   
        $this->form_validation->set_rules('id', 'id', 'trim'); 
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
        }

       public function delete_service($id) 
        { 
        $row = $this->Vendors_md->get_by_id('service',$id); 
        if ($row) 
            { 
            $this->Vendors_md->delete('service',$id); 
            $this->session->set_flashdata('message', 'Delete Record Success'); 
            redirect(base_url('Vendors/service')); 
            } 
          else 
            { 
            $this->session->set_flashdata('message', 'Record Not Found'); 
            redirect(base_url('Vendors/service'));  
            } 
        }

        public function sub_service($id){
           $service = $this->Vendors_md->get_all_sub('sub_service',$id); 
           $data = array( 
               'service_data' => $service 
           ); 
            $this->load->template('list_of_sub',$data);
            //$this->load->view('welcome_message');
        }

        public function add_sub_service(){
            $service = $this->Vendors_md->get_all('service');  
            $data = array( 
                'button' => 'Create' ,
                'action' => base_url('Vendors/add_sub_service_action') , 
                'id' =>  '', 
                'name' =>  '',
                'service' => '',
                'services' => $service
            ); 
            $this->load->template('add_sub_service',$data);
        }
        public function add_sub_service_action(){
            $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
            $this->form_validation->set_rules('service', 'service', 'trim|required');  
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
            if ($this->form_validation->run() == FALSE) 
                { 
                $this->add_sub_service(); 
                } 
            else 
                { 
                $data = array( 
                    'name' => $this->input->post('name', TRUE) ,  
                    'created_at' => date('Y-m-d H:i:s') , 
                    'service_id' => $this->input->post('service', TRUE) ,  
                ); 
                $this->Vendors_md->insert('sub_service',$data); 
                $this->session->set_flashdata('message', 'Create Record Success'); 
                redirect(base_url('Vendors/service')); 
                }
        }
        public function update_sub_service($id) 
        { 
            $service = $this->Vendors_md->get_all('service');
            $row = $this->Vendors_md->get_by_id('sub_service',$id); 
        if ($row) 
            { 
            $data = array( 
                'button' => 'Update', 
                'action' => base_url('Vendors/update_sub_service_action') , 
                'id' =>  $row->id, 
                'name' => $row->name,  
                'services' =>$service,
                'service' =>$row->service_id
            );
            $this->load->template('add_sub_service', $data); 
            } 
        else 
            { 
            $this->session->set_flashdata('message', 'Record Not Found'); 
            redirect(base_url('Vendors/service')); 
            } 
        }

        public function update_sub_service_action() 
        { 
            $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
            $this->form_validation->set_rules('service', 'service', 'trim|required');  
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
        if ($this->form_validation->run() == FALSE) 
            { 
            $this->update_service($this->input->post('id', TRUE)); 
            } 
          else 
            { 
            $data = array( 
                'name' => $this->input->post('name', TRUE) , 
                'service_id' => $this->input->post('service', TRUE) ,  
                // 'email' => $this->input->post('email', TRUE) , 
                // 'phone' => $this->input->post('phone', TRUE) , 
                // 'created_at' => date('Y-m-d H:i:s') , 
            ); 
            $this->Vendors_md->update_service('sub_service',$this->input->post('id', TRUE) , $data); 
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Vendors/service')); 
            } 
        }
        public function delete_sub_service($id) 
        { 
        $row = $this->Vendors_md->get_by_id('sub_service',$id); 
        if ($row) 
            { 
            $this->Vendors_md->delete('sub_service',$id); 
            $this->session->set_flashdata('message', 'Delete Record Success'); 
            redirect(base_url('Vendors/service')); 
            } 
          else 
            { 
            $this->session->set_flashdata('message', 'Record Not Found'); 
            redirect(base_url('Vendors/service'));  
            } 
        }


        // business modules  


        public  function business()
        {
          
           $service = $this->Vendors_md->get_all('business'); 
           $data = array( 
               'service_data' => $service 
           ); 
            $this->load->template('business_list',$data);
            //$this->load->view('welcome_message');
        }

        public function create_business() 
        {  
        $data = array( 
            'button' => 'Create' ,
            'action' => base_url('Vendors/add_business') , 
            'id' =>  '', 
            'name' =>  '',
            'description' =>  ''
        ); 
        $this->load->template('add_business',$data);
        }

    public function add_business() 
    { 
    $this->_rulese(); 
    if ($this->form_validation->run() == FALSE) 
        { 
        $this->create_business(); 
        } 
      else 
        { 
        $data = array( 
            'name' => $this->input->post('name', TRUE) ,  
            'description' => $this->input->post('description', TRUE) ,  
            'created_at' => date('Y-m-d H:i:s') , 
        ); 
        $this->Vendors_md->insert('business',$data); 
        $this->session->set_flashdata('message', 'Create Record Success'); 
        redirect(base_url('Vendors/business')); 
        } 
    }

    public function update_business($id) 
    { 
    $row = $this->Vendors_md->get_by_id('business',$id); 
    if ($row) 
        { 
        $data = array( 
            'button' => 'Update', 
            'action' => base_url('Vendors/update_business_action') , 
            'id' =>  $row->id, 
            'name' => $row->name,  
            'description' => $row->description,  
        ); 
        $this->load->template('add_business', $data); 
        } 
      else 
        { 
        $this->session->set_flashdata('message', 'Record Not Found'); 
        redirect(base_url('Vendors/business')); 
        } 
    }

    public function update_business_action() 
        { 
        $this->_rulese(); 
        if ($this->form_validation->run() == FALSE) 
            { 
            $this->update_business($this->input->post('id', TRUE)); 
            } 
          else 
            { 
            $data = array( 
                'name' => $this->input->post('name', TRUE) , 
                'description' => $this->input->post('description', TRUE) , 
                // 'email' => $this->input->post('email', TRUE) , 
                // 'phone' => $this->input->post('phone', TRUE) , 
                // 'created_at' => date('Y-m-d H:i:s') , 
            ); 
            $this->Vendors_md->update_service('business',$this->input->post('id', TRUE) , $data); 
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Vendors/business')); 
            } 
        }

 

       public function delete_business($id) 
        { 
        $row = $this->Vendors_md->get_by_id('business',$id); 
        if ($row) 
            { 
            $this->Vendors_md->delete('business',$id); 
            $this->session->set_flashdata('message', 'Delete Record Success'); 
            redirect(base_url('Vendors/business')); 
            } 
          else 
            { 
            $this->session->set_flashdata('message', 'Record Not Found'); 
            redirect(base_url('Vendors/business'));  
            } 
        }


}

?>