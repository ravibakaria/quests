

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Sub Service List</h2> 
        </div> 
        <div class="col-md-4 text-center"> 
            <div style="margin-top: 4px" id="message"> 
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?> 
            </div> 
        </div> 
        
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Service Name</th> 
                <th>Sub Service Name</th> 
                <th>Added On</th> 
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($service_data as $service) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <?php echo $service->servicename ?> 
                    </td>  
                    <td> 
                        <?php echo $service->name ?> 
                    </td>  
                    <td> 
                        <?php echo $service->created_at ?> 
                    </td> 
                    <td style="text-align:center" width="200px"> 
                        <?php  
            echo anchor(base_url('Vendors/Vendors/update_sub_service/'.$service->id),'Update');  
            echo ' | ';  
            echo anchor(base_url('Vendors/Vendors/delete_sub_service/'.$service->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');  
            ?> 
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>