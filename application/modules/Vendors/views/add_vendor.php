
    <style> 
        body { 
            padding: 15px; 
        } 

       
    </style> 
</head> 
<body> 
    <h2 style="margin-top:0px">Staff <?php echo $button ?></h2> 
    <form action="<?php echo $action; ?>" method="post"> 
        <div class="form-group"> 
            <label for="varchar">Name 
                <?php echo form_error('name') ?> 
            </label> 
            <input type="text" class="form-control" name="name" id="job_position" placeholder="name" value="<?php echo $name; ?>" /> 
        </div> 
        <div class="form-group"> 
            <label for="varchar">Email 
                <?php echo form_error('email') ?> 
            </label> 
            <input type="text" class="form-control" name="email" id="description" placeholder="email" value="<?php echo $email; ?>" /> 
        </div>
        <div class="form-group"> 
            <label for="varchar">Phone No 
                <?php echo form_error('phone') ?> 
            </label> 
            <input type="text" class="form-control" name="phone" id="description" placeholder="phone" value="<?php echo $phone; ?>" /> 
        </div>
        <?php if($pass == ''){?>
        <div class="form-group"> 
            <label for="varchar">Password 
                <?php echo form_error('Password') ?> 
            </label> 
            <input type="password" class="form-control" min="5" name="password" id="description" placeholder="Password" /> 
        </div>
        <?php } ?>
        <div class="form-group"> 
        <label for="">Role
        <?php echo form_error('role') ?> </label>
        <select name="role" class="form-control" style = "height: 34px"> 
            <option value="">Select</option>
            <?php
            foreach($roles as $value)
             {
                 if($value->id == $role)
                 {
                     echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
                }
                 else
                 {
                     echo "<option value='".$value->id."'>".$value->name."</option>";
                }
            }
            ?>
        </select>     
        </div>
        <div class="form-group"> 
        <label for="">Status</label>
        <select name="status" class="form-control" style = "height: 34px"> 
        <?php ?>
            <option <?php if($status == 1){ echo "selected='selected'";}?> value='1'>Active</option>";
            <option <?php if($status == 0){ echo "selected='selected'";}?>value="0">InActive</option>
        </select>     
        </div>
        <p class="font-weight-bold">Basic Access</p>
        <?php echo form_error('access') ?> </label>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
      <!-- Default checked -->
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check1"  name = 'access[]' value="dashboard" <?php if(isset($rolasses[0]->name) == 'dashboard' ){echo "checked";}?> >
        <label class="custom-control-label" for="check1">Dashboard</label>
      </div>
    </li>
    <li class="list-group-item">
      <!-- Default checked -->
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check2" name = 'access[]' value="e-commerce" <?php if(isset($rolasses[1]->name) == 'e-commerce' ){echo "checked";}?>>
        <label class="custom-control-label" for="check2">E-commerce</label>
      </div>
    </li>
    <li class="list-group-item">
      <!-- Default checked -->
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check3" name = 'access[]' value="staff_management" <?php if(isset($rolasses[2]->name) == 'staff_management' ){echo "checked";}?> >
        <label class="custom-control-label" for="check3">Staff Management</label>
      </div>
    </li>
    <li class="list-group-item">
      <!-- Default checked -->
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check4" name = 'access[]' value="service" <?php if(isset($rolasses[3]->name) == 'service' ){echo "checked";}?> >
        <label class="custom-control-label" for="check4">Service</label>
      </div>
    </li>
    <li class="list-group-item">
      <!-- Default checked -->
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check5" name = 'access[]' value="report" <?php if(isset($rolasses[4]->name) == 'report' ){echo "checked";}?> >
        <label class="custom-control-label" for="check5">Report</label>
      </div>
    </li>
  

        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
        <button type="submit" class="btn btn-primary"> 
            <?php echo $button ?> 
        </button> 
        <a href="<?php echo base_url('Vendors/Vendors') ?>" class="btn btn-default">Cancel</a> 
    </form> 
</body> 
</html>