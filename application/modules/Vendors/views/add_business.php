 
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <h2 style="margin-top:0px">Business Modules <?php echo $button ?></h2> 
    <form action="<?php echo $action; ?>" method="post"> 
        <div class="form-group"> 
            <label for="varchar">Name 
                <?php echo form_error('name') ?> 
            </label> 
            <input type="text" class="form-control" name="name" id="job_position" placeholder="name" value="<?php echo $name; ?>" /> 
        </div>  
        <div class="form-group"> 
            <label for="varchar">Description 
                <?php echo form_error('description') ?> 
            </label> 
            <textarea name="description" id="" cols="30" rows="5" class="form-control" name="description" id="description" placeholder="description" ><?php echo $description; ?></textarea>
            
        </div>  
        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
        <button type="submit" class="btn btn-primary"> 
            <?php echo $button ?> 
        </button> 
        <a href="<?php echo base_url('Vendors/business') ?>" class="btn btn-default">Cancel</a> 
    </form> 
</body> 
</html>