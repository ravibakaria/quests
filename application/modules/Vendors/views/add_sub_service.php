    
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <h2 style="margin-top:0px">Sub Service <?php echo $button ?></h2> 
    <form action="<?php echo $action; ?>" method="post"> 
        <div class="form-group"> 
            <label for="varchar">Name 
                <?php echo form_error('name') ?> 
            </label> 
            <input type="text" class="form-control" name="name" id="job_position" placeholder="name" value="<?php echo $name; ?>" /> 
        </div>
        <div class="form-group"> 
            <label for="varchar">Service 
                <?php echo form_error('service') ?> 
            </label> 
        <select name="service" class="form-control" style = "height: 34px"> 
            <option value="">Select</option>
            <?php
            foreach($services as $value)
             {
                 if($value->id == $service)
                 {
                     echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
                }
                 else
                 {
                     echo "<option value='".$value->id."'>".$value->name."</option>";
                }
            }
            ?>
        </select> 
        </div>  
        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
        <button type="submit" class="btn btn-primary"> 
            <?php echo $button ?> 
        </button> 
        <a href="<?php echo base_url('Vendors/service') ?>" class="btn btn-default">Cancel</a> 
    </form> 
</body> 
</html>