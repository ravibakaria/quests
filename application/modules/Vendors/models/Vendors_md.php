<?php 
class Vendors_md extends CI_Model 
 
    { 
    //public $table = 'vendor'; 
 
    public $id = 'id'; 
 
    public $order = 'DESC'; 
 
    function __construct() 
        { 
        parent::__construct(); 
        } 
 
    // get all 
 
    function get_all($table) 
        { 
        $this->db->order_by($this->id, $this->order); 
        return $this->db->get($table)->result(); 
        } 
  
    function get_all_bm($table,$id) { 
        $this->db->where('sevice_id', $id);  
        return $this->db->get($table)->result(); 
    } 
  
    // get data by id 
 
    function get_by_id($table,$id) 
        { 
        $this->db->where($this->id, $id); 
        return $this->db->get($table)->row(); 
        } 
    function get_by_id_array($table,$id) 
        { 
            $this->db->where('user_id', $id); 
        return $this->db->get($table)->result(); 
        } 
    // insert data 
 
    function insert($table,$data) 
        { 
        $this->db->insert($table, $data); 
        $insert_id = $this->db->insert_id();

         return  $insert_id;
        } 
 
    // update data 
 
    function update($table,$id, $data) 
        { 
        $this->db->where($this->id, $id); 
        $this->db->update($table, $data); 
        } 
 
    // delete data 
 
    function delete($table,$id) 
        { 
        $this->db->where($this->id, $id); 
        $this->db->delete($table); 
        }
    function deleteasses($table,$id) 
        { 
        $this->db->where('user_id', $id); 
        $this->db->delete($table); 
        }
    function deletebusiness_module($table,$id) 
        { 
        $this->db->where('sevice_id', $id); 
        $this->db->delete($table); 
        }
        
        // updateService data 
 
    function update_service($table,$id, $data) 
    { 
    $this->db->where($this->id, $id); 
    $this->db->update($table, $data); 
    } 

    // deleteervice data 
 
    function delete_service($table,$id) 
        { 
        $this->db->where($this->id, $id); 
        $this->db->delete($table); 
        }
    
    function get_vendor_service(){
        $this->db->select('vendor.*,service.name as servicename');
        $this->db->from('vendor');
        $this->db->join('service', 'vendor.service = service.id', 'left'); 
        $this->db->order_by('vendor.id', $this->order);
        $query = $this->db->get();
        return $query->result();

    }
    function get_all_vendor_service(){
        $this->db->select('admin.*,roles.name as rolename');
        $this->db->from('admin');
        $this->db->join('roles', 'admin.role = roles.id', 'left'); 
        $this->db->order_by('admin.id', $this->order);
        $query = $this->db->get();
        return $query->result();

    }

    function get_all_sub($table, $id) 
    { 
        $this->db->select('sub_service.*,service.name as servicename');
        $this->db->from('sub_service');
        $this->db->join('service', 'service.id = sub_service.service_id', 'left'); 
        $this->db->where('service_id', $id); 
        $this->db->order_by('sub_service.id', $this->order);
        $query = $this->db->get();
        return $query->result();
   
    // $this->db->order_by($this->id, $this->order); 
    // return $this->db->get($table)->result(); 
    } 
}