<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <!-- <link href="//cdnjs.cloudflare.com/ajax/libs/bulma/0.4.1/css/bulma.min.css" rel="stylesheet" id="bootstrap-css"> -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Mukta');
        body{
        font-family: 'Mukta', sans-serif;
            height:100vh;
            min-height:550px;
            /* background-image: url(http://www.planwallpaper.com/static/images/Free-Wallpaper-Nature-Scenes.jpg); */
            background-repeat: no-repeat;
            background-size:cover;
            background-position:center;
            position:relative;
            overflow-y: hidden;
        }
        a{
        text-decoration:none;
        color:#444444;
        }
        .login-reg-panel{
            position: relative;
            top: 50%;
            transform: translateY(-50%);
            text-align:center;
            width:70%;
            right:0;left:0;
            margin:auto;
            height:400px;
            background-color: rgba(236, 48, 20, 0.9);
        }
        .white-panel{
            background-color: rgba(255,255, 255, 1);
            height:500px;
            position:absolute;
            top:-50px;
            width:50%;
            right:calc(50% - 50px);
            transition:.3s ease-in-out;
            z-index:0;
            box-shadow: 0 0 15px 9px #00000096;
        }
        .login-reg-panel input[type="radio"]{
            position:relative;
            display:none;
        }
        .login-reg-panel{
            color:#B8B8B8;
        }
        .login-reg-panel #label-login, 
        .login-reg-panel #label-register{
            border:1px solid #9E9E9E;
            padding:5px 5px;
            width:150px;
            display:block;
            text-align:center;
            border-radius:10px;
            cursor:pointer;
            font-weight: 600;
            font-size: 18px;
        }
        .login-info-box{
            width:30%;
            padding:0 50px;
            top:20%;
            left:0;
            position:absolute;
            text-align:left;
        }
        .register-info-box{
            width:30%;
            padding:0 50px;
            top:20%;
            right:0;
            position:absolute;
            text-align:left;
            
        }
        .right-log{right:50px !important;}

        .login-show, 
        .register-show{
            z-index: 1;
            display:none;
            opacity:0;
            transition:0.3s ease-in-out;
            color:#242424;
            text-align:left;
            padding:50px;
        }
        .show-log-panel{
            display:block;
            opacity:0.9;
        }
        .login-show input[type="text"], .login-show input[type="password"]{
            width: 100%;
            display: block;
            margin:20px 0;
            padding: 15px;
            border: 1px solid #b5b5b5;
            outline: none;
        }
        .login-show input[type="button"] {
            max-width: 150px;
            width: 100%;
            background: #444444;
            color: #f9f9f9;
            border: none;
            padding: 10px;
            text-transform: uppercase;
            border-radius: 2px;
            float:right;
            cursor:pointer;
        }
        .login-show a{
            display:inline-block;
            padding:10px 0;
        }

        .register-show input[type="text"], .register-show input[type="password"]{
            width: 100%;
            display: block;
            margin:20px 0;
            padding: 15px;
            border: 1px solid #b5b5b5;
            outline: none;
        }
        .register-show input[type="button"] {
            max-width: 150px;
            width: 100%;
            background: #444444;
            color: #f9f9f9;
            border: none;
            padding: 10px;
            text-transform: uppercase;
            border-radius: 2px;
            float:right;
            cursor:pointer;
        }
        .credit {
            position:absolute;
            bottom:10px;
            left:10px;
            color: #3B3B25;
            margin: 0;
            padding: 0;
            font-family: Arial,sans-serif;
            text-transform: uppercase;
            font-size: 12px;
            font-weight: bold;
            letter-spacing: 1px;
            z-index: 99;
        }
        a{
        text-decoration:none;
        color:#2c7715;
        }
        .or-seperator {
        margin-top: 20px;
        text-align: center;
        border-top: 1px solid #ccc;
        }
        .social-btn .btn {
            border: none;
            margin: 10px 3px 0;
            opacity: 1;
        }
        .social-btn .btn:hover {
            opacity: 0.9;
        }
        .social-btn .btn-primary {
            background: #507cc0;
            height: 39px;
            width: 114px;
        }
        }
        .social-btn .btn-info {
            background: #64ccf1;
        }
        .social-btn .btn-danger {
            background: #df4930;
            height: 39px;
            width: 114px;
        }
        }
        .or-seperator i {
            padding: 0 10px;
            background: #f7f7f7;
            position: relative;
            top: -11px;
            z-index: 1;
        }
        .modal-footer {   border-top: 0px; }
        .showSweetAlert {margin-top: -290px !important;}

        @media only screen and (max-width: 700px) {
            body {
                background-color: lightblue;
            }
            .white-panel {
                width: 100%;
                position: static;
            }
            .register-info-box {
                display: none;
                opacity: 0;
                
            }
            .login-info-box {
                display: none;
                opacity: 0;
            }
            .register-info-box1 {
                display: block !important;;
                
            }
            .login-info-box1 {
                display: block !important;;  
            }
        }
        @media only screen and (max-width: 1900px) {
            .register-info-box1 {
                display: none;
                
            }
            .login-info-box1 {
                display: none;  
            }
        }
    </style>
</head>
<body>

<!------ Include the above in your HEAD tag ---------->

<div class="login-reg-panel">
		<div class="login-info-box">
			<h2>Have an account?</h2>
			<p>Lorem ipsum dolor sit amet</p>
			<label id="label-register" for="log-reg-show">Login</label>
			<input type="radio" name="active-log-panel" id="log-reg-show"  checked="checked">
		</div>
							
		<div class="register-info-box">
			<h2>Don't have an account?</h2>
			<p>Lorem ipsum dolor sit amet</p>
			<label id="label-login" for="log-login-show">Register</label>
			<input type="radio" name="active-log-panel" id="log-login-show">
		</div>
							
		<div class="white-panel">
			<div class="login-show">
				<h2>LOGIN</h2>
				<input type="text" id ="email" placeholder="Email" autocomplete="off">
				<input type="password" id = "pass" placeholder="Password" autocomplete="off">
				<input type="button" id="login" value="Login">
				<a href="#" data-target="#pwdModal" data-toggle="modal">Forgot password?</a>
                <div class="register-info-box1">
                    <h4>Don't have an account?</h4>
                    <label id="label-login" for="log-login-show">Register</label>
                    <input type="radio" name="active-log-panel" id="log-login-show">
                </div>
                <div class="or-seperator"><i style = " padding: 0 10px;
            background: #f7f7f7;
            position: relative;
            top: -11px;
            z-index: 1;">or</i></div>
                <p class="text-center">Login with your social media account</p>
                <div class="text-center social-btn">
                    <a href="<?php echo base_url();?>login/fblogin" class="btn btn-primary"><i class="fa fa-facebook"></i>&nbsp; Facebook</a>
                    <a  href="#" class="button is-info btn btn-primary is-loading">Laoding Button</a>
                    <a href="#" class="btn btn-danger"><i class="fa fa-google"></i>&nbsp; Google</a>
                </div>
                <footer class="modal-row"><div class="modal-row-inner has-big_padding form-actions"><a class="button is-google is-green_candy is-medium is-full_width" rel="nofollow" data-method="post" href="/users/auth/google"><img src="https://assets.toptal.io/assets/front/static/platform/icons/social/google_30739e.svg"><span>Log in with Google</span></a></div></footer>
			</div>
			<div class="register-show">
				<h2>REGISTER</h2>
				<input type="text"  placeholder="Email" id = 'remail'>
				<input type="text"  placeholder="Name" id = 'name'>
				<input type="password"  placeholder="Password" id ='rpass'>
				<input type="password" placeholder="Confirm Password" id = 'rcpass'>
				<input type="button" value="Register" id = 'rbutton'>
                <div class="login-info-box1">
                    <h4>Have an account?</h4>
                    <label id="label-register" for="log-reg-show">Login</label>
                    <input type="radio" name="active-log-panel" id="log-reg-show"  checked="checked">
                </div>
			</div>
		</div>
	</div>
                
    <div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div> -->
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                <div class="text-center">
                                <h3><i class="fa fa-lock fa-4x"></i></h3>
                                <h2 class="text-center">Forgot Password?</h2>
                                <p>You can reset your password here.</p>
                                    <div class="panel-body">
                                        <fieldset>
                                            <div class="form-group">
                                                <input class="form-control input-lg" placeholder="E-mail Address" name="email" type="email" autocomplete="off">
                                            </div>
                                            <input class="btn btn-lg btn-primary btn-block" value="Send My Password" type="submit">
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 text-right">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    </div>	
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>
    <script>

$(document).ready(function(){
    $('.login-info-box').fadeOut();
    $('.login-show').addClass('show-log-panel');
});


$('.login-reg-panel input[type="radio"]').on('change', function() {
    if($('#log-login-show').is(':checked')) {
        $('.register-info-box').fadeOut(); 
        $('.login-info-box').fadeIn();
        
        $('.white-panel').addClass('right-log');
        $('.register-show').addClass('show-log-panel');
        $('.login-show').removeClass('show-log-panel');
        
    }
    else if($('#log-reg-show').is(':checked')) {
        $('.register-info-box').fadeIn();
        $('.login-info-box').fadeOut();
        
        $('.white-panel').removeClass('right-log');
        
        $('.login-show').addClass('show-log-panel');
        $('.register-show').removeClass('show-log-panel');
    }
});

$('#login').on('click', function() {
    var email = $("#email").val();
    var pass = $("#pass").val();
    if(email == ''){
        alert ("Email is requred");
        return false;
    }
    if( pass == ''){
        alert ("Password is requred");
        return false;
    }
    $.ajax({
            url: "<?php echo base_url('login/userlogin');?>",
            method: "POST",
            data: {email: email, pass: pass},
            cache: true
        }).success(function(data){
            data = $.parseJSON(data);
            if(data.status == 1){
                swal({
                title: "DONE",
                text: "Your are Successfully Login!",
                type: "success",
                confirmButtonClass: "btn-success",
                });
                setTimeout(function(){ location.href="Home"; }, 3000);
                
            }else{
                swal("Fail!", "Invalid Email or Password!", "error");
            }
           
        }).fail(function(data){
            swal("Fail!", "Invalid Email or Password!", "error");
        });
});

$('#rbutton').on('click', function() {
    var remail = $("#remail").val();
    var rpass = $("#rpass").val();
    var rcpass = $("#rcpass").val();
    var name = $("#name").val();
    if(remail == ''){
        alert ("Email is requred");
        return false;
    }
    if( rpass == ''){
        alert ("Password is requred");
        return false;
    }
    if( rcpass == ''){
        alert ("Confirm Password is requred");
        return false;
    } 
    if( rpass != rcpass){
        alert ("Password and Confirm Not Match");
        return false;
    }
    if( name == ''){
        alert ("Password and Confirm Not Match");
        return false;
    }
    $.ajax({
            url: "<?php echo base_url('login/userRegiseter');?>",
            method: "POST",
            data: {name:name,email: remail, pass: rpass, passconf: rcpass},
            cache: true
        }).success(function(data){
            data = $.parseJSON(data);
            if(data.status == 1){
                swal({
                title: "DONE",
                text: "Your are Successfully Register!",
                type: "success",
                confirmButtonClass: "btn-success",
                });
                //setTimeout(function(){ location.href="login"; }, 3000);
                
            }else{
                swal("Fail!", data.status, "error");
            }
           
        }).fail(function(data){
            swal("Fail!", data.status, "error");
        });
});
    </script>
</body>
</html>