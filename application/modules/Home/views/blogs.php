<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
    <div class="container">
        
        <?php 
        if(!empty($blogs)){
        foreach($blogs as $blog){?>
            <article class="card card-product-list">
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    
                        <h2><?php echo $blog->heading?></h2>
                  
                    <div class="card-body">
                    <?php echo $blog->body?>
                    <p> <i class='far fa-calendar-alt' style='font-size:20px'></i><?php echo " ".$blog->date?></p>
                    </div>
                </div>
            </div>
            </article> 
        <?php } } else{?>
            <article class="card card-product-list">
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                  <h3>No Posts Yet</h3>
                </div>
            </div>
            </article> 
        <?php } ?>
    </div> 
</section>
<!-- =============== SECTION ITEMS =============== -->