<?php 
        if(!empty($products)){
        foreach($products as $product){?>
            <article class="card card-product-list">
                <div class="row no-gutters">
                    <aside class="col-md-3">
                        <a href="<?php echo base_url('User/product_details/').$product->id;?>" class="img-wrap">
                        <?php $images = $this->db->get_where('images',['product_id'=>$product->id])->row(); ?>
                            <!-- <span class="badge badge-danger"> </span> -->
                            <img src="<?php echo base_url('assets/images/'.$images->img_name);?>">
                        </a>
                    </aside> <!-- col.// -->
                    <div class="col-md-6">
                        <div class="info-main">
                            <a href="#" class="h5 title"> <?php echo ucfirst($product->name)?></a>
                            <p> <?php echo ucfirst($product->description)?></p>
                        </div> <!-- info-main.// -->
                    </div> <!-- col.// -->
                    <aside class="col-sm-3">
                        <div class="info-aside">
                            <div class="price-wrap">
                                <del><span class="h5 price"><?php if(!empty($product->showpries)){ echo "&#x20B9;".$product->showpries." " ;}?></span> </del>
                                <span class="h5 price"><?php echo "&#x20B9;".$product->pries?></span>
                                <small class="text-muted">/per item</small>
                            </div> <!-- price-wrap.// -->
                            <p class="mt-3">
                            <!-- <form method = "post" action="<?= base_url().'User/add_cart'?>">
			
                                <input type="hidden" name="id" value= "<?= $product->id?>">
                                <input type="hidden" class="form-control" value="1" name="qty">
                                <div class="form-group col-md">
                                    <button class="btn btn-primary" type = "submit"> 
                                        <i class="fa fa-shopping-cart"></i><span class="text">Add to cart</span> 
                                    </button> 
                                </div> 
                            </form> -->
                            <a href="<?php echo base_url('User/product_details/'.$product->id)?>" class="btn popularbtn btn-sm"> Source now </a>
                            </p>
                        </div> <!-- info-aside.// -->
                    </aside> <!-- col.// -->
                </div> <!-- row.// -->
            </article> <!-- card-product .// -->
<?php } }?>