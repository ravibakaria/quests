<footer>
    <div class="top text-center">
      <a href="#nav-top">Back to top</a>
    </div>

    <div class="bottom">
      <div class="center">
       

        <ul class="copy text-center">
          <li><a href="#">Conditions of Use</a></li>
          <li><a href="#">Privacy Notice</a></li>
          <li><a href="#">Interest-Based Ads</a></li>
          <li>&copy; 1996-2020, Quests.co.in, Inc. or its affiliates</li>
        </ul>
      </div>
    </div>
  </footer>

  <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src=" <?php echo base_url('assets/js/app.js');?>"></script>
</body>

</html>