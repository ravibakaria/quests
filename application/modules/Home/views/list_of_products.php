<!-- ========================= SECTION CONTENT ========================= -->
<style>
.popularbtn {
    background-color: #FF4501;
    margin-top: 23px;
}
</style>
<section class="section-content padding-y">
    <div class="container">
        <div class="card mb-3">
            <div class="card-body">
                <ol class="breadcrumb float-left">
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Home/show_all_produuct_categories/').$categoryname->id;?>"><?php echo ucfirst($categoryname->name)?></a></li>
                    <?php if(!empty($subservicename)){?>
                    <li class="breadcrumb-item"><a href="#"><?php echo ucfirst($subservicename->name)?></a></li>
                    <?php } ?>
                </ol>
            </div> <!-- card-body .// -->
        </div>


        <div class="row">
	<aside class="col-md-2">

	<article class="filter-group">
		<h6 class="title">
			<a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#collapse_1"><?php echo ucfirst($categoryname->name)?></a>
		</h6>
		<div class="filter-content collapse show" id="collapse_1" style="">
			<div class="inner" style ="height: 400px;overflow-y: scroll;">
				<ul class="list-menu">
                    <?php if(!empty($subservice)){
                        foreach($subservice as $sub){?>
                            <li><a href="<?php echo base_url('Home/show_all_produuct_categories/').$categoryname->id.'/'.$sub->id;?>"><?php echo ucfirst($sub->name) ?></a></li>
                       <?php }
                    }?>
					
				</ul>
			</div> <!-- inner.// -->
		</div>
	</article> <!-- filter-group  .// -->
	<!-- <article class="filter-group">
		<h6 class="title">
			<a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#collapse_2"> Brands </a>
		</h6>
		<div class="filter-content collapse show" id="collapse_2">
			<div class="inner">
				<label class="custom-control custom-checkbox">
				  <input type="checkbox" checked="" class="custom-control-input">
				  <div class="custom-control-label">Adidas  
				  	<b class="badge badge-pill badge-light float-right">120</b>  </div>
				</label>
				<label class="custom-control custom-checkbox">
				  <input type="checkbox" checked="" class="custom-control-input">
				  <div class="custom-control-label">Nike 
				  	<b class="badge badge-pill badge-light float-right">15</b>  </div>
				</label>
				<label class="custom-control custom-checkbox">
				  <input type="checkbox" checked="" class="custom-control-input">
				  <div class="custom-control-label">The Noth Face 
				  	<b class="badge badge-pill badge-light float-right">35</b> </div>
				</label>
				<label class="custom-control custom-checkbox">
				  <input type="checkbox" checked="" class="custom-control-input">
				  <div class="custom-control-label">The cat 
				  	<b class="badge badge-pill badge-light float-right">89</b> </div>
				</label>
				<label class="custom-control custom-checkbox">
				  <input type="checkbox" class="custom-control-input">
				  <div class="custom-control-label">Honda 
				  	<b class="badge badge-pill badge-light float-right">30</b>  </div>
				</label>
			</div> 
		</div>
	</article> filter-group .// -->
    <?php if(!empty($size)){ ?>
    <article class="filter-group">
		<h6 class="title">
			<a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#collapse_4"> Sizes </a>
		</h6>
		<div class="filter-content collapse show" id="collapse_4">
			<div class="inner">
                <?php 
                    foreach($size as $s){ ?>
                        <label class="checkbox-btn">
                            <input type="checkbox" value = "<?php echo $s ?>" class = "sizeval">
                            <span class="btn btn-light"><?php echo $s?> </span>
                        </label>
                        <?php   
                    }
                ?>  
			</div> <!-- inner.// -->
		</div>
	</article> <!-- filter-group .// -->
    <?php } ?>
    <?php            
        if(!empty($color)){ ?>
    <article class="filter-group">
		<h6 class="title">
			<a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#collapse_5"> Colour </a>
		</h6>
		<div class="filter-content collapse show" id="collapse_5">
			<div class="inner">
                <?php
                    foreach($color as $c){ ?>
                        <label class="custom-control custom-radio">
                            <input type="radio" name="myfilter_radio" checked="" class="custom-control-input colorval" value = "<?php echo $c ?>">
                            <div class="custom-control-label"><?php echo $c ?></div>
                        </label>
                        <?php   
                    }
                    
                ?>  
				
			</div> <!-- inner.// -->
		</div>
	</article> <!-- filter-group .// -->
    <?php } ?>
	<article class="filter-group">
		<h6 class="title">
			<a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#collapse_3"> Price range </a>
		</h6>
		<div class="filter-content collapse show" id="collapse_3">
			<div class="inner">
				<input type="range" id = "slider" class="custom-range" min="<?php echo $minpries->pries?>" max="<?php echo $maxpries->pries?>" name="">
				<div class="form-row">
				<div class="form-group col-md-6">
				  <label>Min</label>
				  <input class="form-control" value = "<?php echo $minpries->pries?>" type="number" readonly>
				</div>
				<div class="form-group text-right col-md-6">
				  <label>Max</label>
				  <input class="form-control" id = "maxvalue" value = "<?php echo $maxpries->pries?>" type="number" readonly>
				</div>
				</div> <!-- form-row.// -->
				<button class="btn btn-block btn-primary getbysizebtn">Apply</button>
			</div> <!-- inner.// -->
		</div>
	</article> <!-- filter-group .// -->
	

	</aside> <!-- col.// -->
	<main class="col-md-10">


    <header class="mb-3">
            <div class="form-inline">
                <strong class="mr-md-auto"><?php echo $productcount; ?> Items found </strong>
                <select class="mr-2 form-control flterval">
                    <option>Latest items</option>
                    <option>Trending</option>
                    <option>Most Popular</option>
                    <option>Cheapest</option>
                </select>
                <div class="btn-group">
                    <!-- <a href="page-listing-grid.html" class="btn btn-light" data-toggle="tooltip" title="List view"> 
                        <i class="fa fa-bars"></i></a>
                    <a href="page-listing-large.html" class="btn btn-light active" data-toggle="tooltip" title="Grid view"> 
                        <i class="fa fa-th"></i></a> -->
                </div>
            </div>
    </header><!-- sect-heading -->

    <?php 
        if(!empty($products)){
        foreach($products as $product){?>
            <article class="card card-product-list">
                <div class="row no-gutters">
                    <aside class="col-md-3">
                        <a href="<?php echo base_url('User/product_details/').$product->id;?>" class="img-wrap">
                        <?php $images = $this->db->get_where('images',['product_id'=>$product->id])->row(); ?>
                            <!-- <span class="badge badge-danger"> </span> -->
                            <img src="<?php echo base_url('assets/images/'.$images->img_name);?>">
                        </a>
                    </aside> <!-- col.// -->
                    <div class="col-md-6">
                        <div class="info-main">
                            <a href="#" class="h5 title"> <?php echo ucfirst($product->name)?></a>
                            <p> <?php echo ucfirst($product->description)?></p>
                        </div> <!-- info-main.// -->
                    </div> <!-- col.// -->
                    <aside class="col-sm-3">
                        <div class="info-aside">
                            <div class="price-wrap">
                                <del><span class="h5 price"><?php if(!empty($product->showpries)){ echo "&#x20B9;".$product->showpries." " ;}?></span> </del>
                                <span class="h5 price"><?php echo "&#x20B9;".$product->pries?></span>
                                <small class="text-muted">/per item</small>
                            </div> <!-- price-wrap.// -->
                            <p class="mt-3">
                            <!-- <form method = "post" action="<?php //base_url().'User/add_cart'?>"> -->
			
                                <input type="hidden" name="id" value= "<?= $product->id?>">
                                <input type="hidden" class="form-control" value="1" name="qty">
                                <div class="form-group col-md">
                                    <!-- <button class="btn btn-primary" type = "submit"> 
                                        <i class="fa fa-shopping-cart"></i><span class="text">Add to cart</span> 
                                    </button> -->
                                    <a href="<?php echo base_url('User/product_details/'.$product->id)?>" class="btn popularbtn btn-sm"> Source now </a>
                                </div> 
                            </form>
                            </p>
                        </div> <!-- info-aside.// -->
                    </aside> <!-- col.// -->
                </div> <!-- row.// -->
            </article> <!-- card-product .// -->
<?php } } else{?>
            <div class="text-center">
               <h3> <span class="a-size-medium a-color-base" dir="auto">No Results Found </span></h3><br>
                <span class="a-size-medium a-color-base" dir="auto">Try checking your spelling or use more general terms</span>
                <span class="a-size-medium a-color-base" dir="auto">.</span></div>
        <?php }?>

<div class="loadmore">
</div>

        
    </div> <!-- container .//  -->
</section>
<!-- =============== SECTION ITEMS =============== -->
<section  class="padding-bottom-sm">

<header class="section-heading heading-line">
	<h4 class="title-section text-uppercase">Recommended items</h4>
</header>

<div class="row row-sm">
	<?php
		foreach($recommendeds as $recommend){?>
         <?php $images = $this->db->get_where('images',['product_id'=>$recommend->id])->row(); ?>
			<div class="col-xl-2 col-lg-3 col-md-4 col-6">
				<div  class="card card-sm card-product-grid">
					<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="img-wrap"> <img src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name);?>"> </a>
					<figcaption class="info-wrap">
						<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="title"><?php echo $recommend->name;?></a>
						<div class="price mt-1">&#x20B9;<?php echo $recommend->pries;?></div> <!-- price-wrap.// -->
					</figcaption>
				</div>
			</div> 
			<?php
		}
	?>
	
</div> <!-- row.// -->
</section>
<!-- =============== SECTION ITEMS .//END =============== -->
<nav class="mb-4" aria-label="Page navigation sample">
  <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">4</a></li>
    <li class="page-item"><a class="page-link" href="#">5</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>


<script> 
$(document).on('input', '#slider', function() {
   
    $('#maxvalue').val( $(this).val() );
});

$(document).on('click', '.getbysizebtn', function() {
    var prize = $('#maxvalue').val();
    var size = $('.sizeval:checked').val();
    var color = $('.colorval:checked').val();
    var flter = $('.flterval').val();
    alert ("Prize = "+prize+" size = "+size+" clor = "+color+" flter = "+flter)
  
});

var count = 1;
$(document).scroll(function(e){


var scrollAmount = $(window).scrollTop();
var documentHeight = $(document).height();


var scrollPercent = (scrollAmount / documentHeight) * 100;

if(scrollPercent > 65 && scrollPercent < 67) {
   
    var prize = $('#maxvalue').val();
    var size = $('.sizeval:checked').val();
    var color = $('.colorval:checked').val();
    var flter = $('.flterval').val();
    Url = "<?php echo base_url('Home/loadmore'); ?>";
    var id = "<?php echo $id?>";
    var subid = "<?php echo $subid?>";
    
    $.ajax({
        type: "POST",
        url: Url,
        data: {prize : prize, size:size, color: color,count:count,id:id,subid:subid},
        success: function (response) {
           
               //alert(response);
               $(".loadmore").append(response);
              // location.href= "<?php echo base_url('Home/addbooking1');?>"
            
        }
    });
    count++;
   
}


});
</script>
