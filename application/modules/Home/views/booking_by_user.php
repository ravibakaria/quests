<!-- ========================= SECTION CONTENT ========================= -->
<?php if($this->session->flashdata('message')){?>
    <div class="alert alert-success">
        <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
    </div>
<?php } ?>
<?php if($this->session->flashdata('emessage')){?>
    <div class="alert alert-danger">
        <strong>Errpr!</strong> <?php echo $this->session->flashdata('emessage');?>.
    </div>
<?php } ?>
<section class="section-content padding-y">
    <div class="container">
       
        
            <article class="card card-product-list">
                <div class="row no-gutters">
                    <aside class="col-md-3">
                        <a href="#" class="img-wrap">
                            <!-- <span class="badge badge-danger"> </span> -->
                            <img src="<?php echo base_url('assets/images/'.$products->img_name);?>">
                        </a>
                    </aside> <!-- col.// -->
                    <div class="col-md-6">
                        <div class="info-main">
                        <?php $this->db->select('shopname,shopadd,shopno');
                            $this->db->from('bussines_partner');
                            $this->db->where('id',$products->u_id);
                            $name =  $this->db->get()->row(); 
                            ?>
                            <a href="#" class="h5 title"> <?php echo ucfirst($name->shopname)?></a>
                            <p> <?php echo ucfirst($products->discription)?></p>
                        </div> <!-- info-main.// -->
                    </div> <!-- col.// -->
                    <aside class="col-sm-3">
                        <div class="info-aside">
                        
                            <div class="price-wrap">
                                <p class="h5 title">Timeing</p>
                                <span class="h5 price"><?php if(!empty($products->opendays)){ echo $products->opendays." " ;}?></span> <br>
                                <span class="h5 price"><?php echo $products->opentime?></span>
                               
                            </div> <!-- price-wrap.// -->
                            <p class="mt-3">
                            <span class=""><?php echo $name->shopadd?></span>
                            </p>
                        </div> <!-- info-aside.// -->
                    </aside> <!-- col.// -->
                </div> <!-- row.// -->
            </article> <!-- card-product .// -->
        
    </div> <!-- container .//  -->
</section>
<link rel="stylesheet" href="<?php echo base_url('assets/fullcalendar/fullcalendar.min.css')?>" />

<script src="<?php echo base_url('assets/fullcalendar/lib/moment.min.js')?>"></script>
<script src="<?php echo base_url('assets/fullcalendar/fullcalendar.min.js')?>"></script>

<script>
var $j = jQuery.noConflict();
$j(document).ready(function () {
    var calendar = $j('#calendar').fullCalendar({
        editable: false,
        eventColor: 'green',
        events: "<?php echo base_url('Business_partner/fetch_event_for_users/').$products->u_id?>",
        displayEventTime: true,
        eventRender: function (event, element, view) {
            if (event.allDay === 'true') {
                event.allDay = true;
            } else {
                event.allDay = false;
            }
        },
        selectable: false,
        selectHelper: false,
        select: function (start, end, allDay) {
            var title = prompt('Event Title:');

            if (title) {
                var start = $j.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                var end = $j.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");

                $.ajax({
                    url: "<?php echo base_url('Business_partner/add_event')?>",
                    data: 'title=' + title + '&start=' + start + '&end=' + end,
                    type: "POST",
                    success: function (data) {
                        displayMessage("Added Successfully");
                    }
                });
                calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                true
                        );
            }
            calendar.fullCalendar('unselect');
        },
        
        editable: false,
        eventDrop: function (event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'edit-event.php',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                        type: "POST",
                        success: function (response) {
                            displayMessage("Updated Successfully");
                        }
                    });
                },
        // eventClick: function (event) {
        //     var deleteMsg = confirm("Do you really want to delete?");
        //     if (deleteMsg) {
        //         $.ajax({
        //             type: "POST",
        //             url: "<?php echo base_url('Business_partner/delete_event')?>",
        //             data: "&id=" + event.id,
        //             success: function (response) {
        //                 if(parseInt(response) > 0) {
        //                     $j('#calendar').fullCalendar('removeEvents', event.id);
        //                     displayMessage("Deleted Successfully");
        //                 }
        //             }
        //         });
        //     }
        // }

    });
});

function displayMessage(message) {
	    $(".response").html("<div class='success'>"+message+"</div>");
    setInterval(function() { $(".success").fadeOut(); }, 1000);
}
</script>

<style>


#calendar {
   
    margin: 0 auto;
}

.response {
    height: 60px;
}

.success {
    background: #cdf3cd;
    padding: 10px 60px;
    border: #c3e6c3 1px solid;
    display: inline-block;
}
</style>
<div class="card">
    <div class="card-head">
        <h2>
        My Booking / Scheduling
        </h2>
    </div>
    <div class="card-body">
        <div class="response"></div>
        <div id='calendar'></div>
    </div>
    <div class="card-footer">
    <div class="form-row  mt-4">
		<form method="post" action="<?php echo base_url('Home/addbooking'); ?>">
			
			<input type="hidden" name="id" value="<?php echo $products->u_id?>">
			<!-- <input type="hidden" name="mytime" id = "mytime" value=""> -->
			<input type="text" name="start" id="datepicker" class="form-control" placeholder="Date" autocomplete="off" required>
            <input type="time" name="time" id="time" class="form-control" placeholder="Time" autocomplete="off" required> <br> <br>
		    <div class="form-group col-md">
		
			<button class="btn btn-primary" type="submit"> 
				</i><span class="text">Book Now</span> 
			</button>
		
		</div> <!-- col.// -->
	</form>
	</div>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
var $j = jQuery.noConflict();
$j(function() {
    $j( "#datepicker" ).datepicker({
      dateFormat: "yy-mm-dd",
      maxDate: 15,
      minDate:0
    });
  } );

// $j("#time").blur(function(){
//     var name = $("#time").val();
    
//     $("#mytime").val(name+':00');
   
// });
</script>
