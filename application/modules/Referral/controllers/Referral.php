<?php

class Referral extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mymodel');
        $this->load->library('form_validation');
    }

    public  function index()
    {
        if($this->session->userdata('reff') ==''){
            $this->load->view('login'); 
        }else{
            redirect(base_url('Referral/Home'));
        }
    }

    public  function register()
    {   
        
         $this->load->view('register'); 
    }
 
    public function userlogin(){
        if($this->input->post()){
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $data = array();
            $var = $this->Mymodel->login('referrals',$email);
        // print_r($var);
            if(!empty($var)){
                if($var->password == md5($pass)){
                    $this->session->set_userdata('reff',$var);
                    redirect(base_url('Referral/Home'));
                    
                }else{
                    $this->session->set_flashdata('message_r', 'Password invalid');
                    
                    redirect(base_url('Referral'));
                }
            }else{
                $this->session->set_flashdata('message_r', 'invalid Password Or Email');
                redirect(base_url('Referral'));
            }
            
        }
    }
    

public function logout(){
    $this->session->unset_userdata('reff');
    $this->session->sess_destroy();
   // redirect (base_url('Referral'));
    redirect ('http://referrals.quests.co.in/');
    
}

public function register_action(){
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[referrals.email]');
    if ($this->form_validation->run() == false) {
        //echo validation_errors();
        $this->session->set_flashdata('message_r', validation_errors());
        redirect(base_url('Referral/register'));
    }else{
        $merchant_order_id =  rand(10000,99999);
        $data = array(
            'email'=> $_POST['email'],
            'name'=> $_POST['name'],
            'password' => md5($_POST['password']),
            'lname' => $_POST['lname'],
            'phone' =>$_POST['phone'],
            'gender' => $_POST['gender'],
            'city' => $_POST['city'],
            'country' => $_POST['country'],
            'pin' => $_POST['pin'],
            'create_at' => date('Y-m-d H:i:s') ,
            'status' => 1, 
            'referral_code' => $merchant_order_id,
           
        );
        $this->Mymodel->register('referrals',$data);

        redirect(base_url('Referral'));
        redirect('http://referrals.quests.co.in/');
    }  
}
public function profile_action(){
   
    $this->form_validation->set_rules('name', 'Name', 'required');
   
    if ($this->form_validation->run() == false) {
        //echo validation_errors();
        $this->session->set_flashdata('message_r', validation_errors());
        redirect(base_url('Referral/profileactivate'));
    }else{
		$id = $this->session->userdata('reff')->id;
        $data = array(
            'email'=> $_POST['email'],
            'name'=> $_POST['name'],
            'lname' => $_POST['lname'],
            'phone' =>$_POST['phone'],
            'gender' => $_POST['gender'],
            'city' => $_POST['city'],
            'holdername' => $_POST['holdername'],
            'bankname' => $_POST['bankname'],
            'accountno' => $_POST['accountno'],
            'ifsc' => $_POST['ifsc'],
            'ifsc' => $_POST['ifsc'],
            'upi' => $_POST['upi'],
            
            'country' => $_POST['country'],
            'pin' => $_POST['pin'],
            'create_at' => date('Y-m-d H:i:s') , 
           
        );
        $this->Mymodel->update('referrals',$id,$data);
		
		redirect(base_url('Referral/Home'));
    }  
}


    public function Home(){
        if($this->session->userdata('reff') !=''){
			if($this->session->userdata('reff')->status == 0){
				redirect(base_url('Referral/profileactivate'));
            }
            $id = $this->session->userdata('reff')->id;
            $reffcode  = $this->session->userdata('reff')->referral_code;
            $myearning = $this->Mymodel->myearning($reffcode);
            $myearning1 = count($myearning) * 70;
            $bal = 0;
            if(!empty($myearning)){
                $bal = count($myearning) * 70;
            }
            $data = [
                'bal' =>$bal,
                'shop' =>$myearning,
                'myearning'=> $myearning1,
            ];
            $payment = $this->Mymodel->bal_payment($id);
            if(!empty($payment)){
                $amout = $payment->amout;
            }else{
                $amout = 0;
            }
            $bal -= $amout;
            $this->session->set_userdata('bal',$bal);
            $this->load->view('header'); 
            $this->load->view('test',$data); 
            $this->load->view('footer'); 
        }else{
            redirect(base_url('Referral'));
        }
    }


    public function profileactivate(){
		$id  = $this->session->userdata('reff')->id;
		
		$profile = $this->Mymodel->myprofile($id);
		
		$data = [
			'prifile' =>$profile,
			
        ];
        $this->load->view('header');
        $this->load->view('profile',$data); 
        $this->load->view('footer');
	}

	public function status(){
        $this->load->view('header');
        $this->load->view('status');
        $this->load->view('footer');
	}

	private function get_curl_handle($payment_id, $amount)  {
        $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
        $key_id = RAZOR_KEY_ID;
        $key_secret = RAZOR_KEY_SECRET;
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
       // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');
        return $ch;
    }   

	public function callback() {        
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');
            $currency_code = 'INR';
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = '';
            try {                
                $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                  // echo "<pre>";print_r($response_array);exit;

                        //Check success response
                        if ($http_status === 200 and isset($response_array['error']) === false) {
                            $success = true;
						   //$this->insert_oder_items($merchant_order_id);
						   $id = $this->session->userdata('reff')->id;
						   $userData = array(
							 'status' =>1,
							 'referral_code' =>$merchant_order_id,
							);
							$this->Mymodel->update('referrals',$id,$userData);
							$profile = $this->Mymodel->myprofile($id);
							$this->session->set_userdata('reff',$profile);
							
                           $this->payment_order($response_array,$merchant_order_id);
                        } else {
                            $success = false;
                            if (!empty($response_array['error']['code'])) {
                                $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                            }
                        }
                }
                //close connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'OPENCART_ERROR:Request to Razorpay Failed';
            }
            if($success === true) {
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                 }
                if (!$order_info['order_status_id']) {
                    redirect($this->input->post('merchant_surl_id'));
                } else {
                    redirect($this->input->post('merchant_surl_id'));
                }
 
            } else {
                redirect($this->input->post('merchant_furl_id'));
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    } 
    public function success() {
		
        $this->load->view('header');
        $this->load->view('paydone'); 
        $this->load->view('footer');
        //$this->load->Template('success.php',$data);
    }  
    public function failed() {
        $data['title'] = 'Razorpay Failed | TechArise';            
        echo "payment failed";
        //$this->load->Template('failed.php',$data);
    }

	public function payment_order($data,$invoice){
        $data1 = array(
            'ORDERID'=>$invoice,
            'TXNID'=>$data['id'],
            'TXNAMOUNT'=>$data['amount']/100,
            'PAYMENTMODE'=>$data['method'],
            'TXNDATE'=>$data['created_at'],
            'STATUS'=>$data['status'],
            
            'email'=>$data['email'],
            'contact'=>$data['contact'],
            'BANKNAME'=>$data['bank'],
           
            'DATA'=> serialize($data)
        );
        $this->db->insert('payments', $data1);
    }

    public function report(){
        $id = $this->session->userdata('reff')->id;
        $myearning = $this->Mymodel->getpay('referral_pay',$id);

        $data = [
           
            'myearning' => $myearning
        ]; 
        
        $this->load->view('header'); 
        $this->load->view('report',$data); 
        $this->load->view('footer'); 
    }

    public function forgetpassword()
    {
        $this->load->view('forgetpass');
    }
    public function forgetpassaction(){
        $email = $this->input->post('email');
        if($email ==''){
            $this->session->set_flashdata('message_r', 'Enter Email');
            redirect(base_url('Business_partner/forgetpassword'));
        }else{
            $getemail = $this->Mymodel->getemail($email);
            if(empty($getemail)){
                $this->session->set_flashdata('message_r', 'Invalid Email');
                redirect(base_url('Business_partner/forgetpassword'));
            }else{
                $mailemail = $getemail->email;
                $token = openssl_random_pseudo_bytes(16);
                $token = bin2hex($token);
                $data = ['token'=>$token];
                $this->Mymodel->set_token($mailemail,$data);
                $data = array(
                    'username'=> $getemail->name.' '.$getemail->lname,
                    "forget_url" => base_url('Business_partner/setPassword/').$token,
                    );
                $subject='Recover your Quests password';
                $name = $getemail->name.' '.$getemail->lname;
                $email = $mailemail;
                $email_params = array();
                $email_params['to_name'] = $name;
                $email_params['to_email'] = $email;
                $email_params['from_email'] = "no-reply@gmail.com";
                $email_params['subject'] = $subject;
                $emailotpmessage = $this->load->view('forgetpassemail',$data,TRUE);
                $email_params['message'] = $emailotpmessage;
                if($this->mail($email_params)){
                $this->session->set_flashdata('message', 'Mail Send On Your Email');
                redirect(base_url('Business_partner/forgetpassword'));
                }else{
                    $this->session->set_flashdata('message_r', 'Email Not Send');
                    redirect(base_url('Business_partner/forgetpassword'));
                }
    
            }
        }
    
    }
    function mail($params = array()) {
        $this->load->library('phpmailer');
  
       $mailer = $this->phpmailer;
  
       // $mailer
  
       $mailer->IsSMTP(); // telling the class to use SMTP
       $mailer->SMTPDebug = 1;                // enables SMTP debug information (for testing)
       // 1 = errors and messages
       // 2 = messages only
  
       $mailer->SMTPAuth = true;                  // enable SMTP authentication
       $mailer->SMTPSecure = "ssl";                 // sets the prefix to the servier
       $mailer->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
       $mailer->Port = 465;                   // set the SMTP port for the GMAIL server
  
     
       $mailer->Username = APPLICATION_EMAIL;            //edit
       $mailer->Password = APPLICATION_PASS;
  
  
       $mailer->AltBody = "To view the message, please use an HTML compatible email viewer!";
  
       $mailer->From = 'no-reply@gmail.com';
       $mailer->FromName = 'noreply';
      //  $mailer->FromName = "Noreply";
      //  $mailer->From = "Noreply@gmail.com";  //edit
       $mailer->isHTML = true;
  
       if (isset($params['from_name'])) {
           $mailer->FromName = $params['from_name'];
       }
  
       if (isset($params['from_email'])) {
           $mailer->From = $params['from_email'];
       }
  
  
       if (!isset($params['subject'])) {
           throw new Exception("Email: Subject is required", 1);
       }
       if (!isset($params['message'])) {
           throw new Exception("Email: Email body is required", 1);
       }
  
       if (!isset($params['to_email']) || (isset($params['to']) && count($params['to']) == 0)) {
           throw new Exception("Email: Atleast one recipient is required", 1);
       }
  
       if (isset($params['to_email'])) {
           $params['to_name'] = (isset($params['to_name'])) ? $params['to_name'] : $params['to_email'];
           $mailer->AddAddress($params['to_email'], $params['to_name']);
       } else {
           throw new Exception("Email: Multiple recipient is not implemented yet", 1);
       }
  
       if (isset($params['addcc']) && !empty($params['addcc'])) {
           foreach ($params['addcc'] as $email => $name)
               $mailer->AddCC($email, $name);
       }
       if (isset($params['addbcc']) && !empty($params['addbcc'])) {
           foreach ($params['addbcc'] as $email => $name)
               $mailer->AddBCC($email, $name);
       }
  
       if (isset($params['addattachment']) && !empty($params['addattachment'])) {
           foreach ($params['addattachment'] as $key => $path)
               $mailer->AddAttachment($path, $key);
       }
  
       $mailer->Subject = $params['subject'];
       $mailer->MsgHTML($params['message']);
  
       $status = 0;
  
       ob_start();
       $mail_send_status = $mailer->Send();
       $mail_send_errors = ob_get_contents();
       ob_end_clean();
       echo $mail_send_errors;
  
       if ($mail_send_status) {
           $status = 1;
       }else{
           $mailer->ErrorInfo;
           exit;
       }
  
       $mailer->ClearAddresses();
       $mailer->ClearAttachments();
  
       return $status;
   }    
}

?>