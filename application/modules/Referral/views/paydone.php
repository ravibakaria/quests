<section class="section-content padding-y">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card text-center bg-danger text-white mb-3">
                <div class="card-body">
                <h4 class="display-4">
                    Your Account Is Activate
                </h4>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card text-center bg-primary text-white mb-3">
                <div class="card-body">
                <h4 class="display-4">
             
                  <a href="<?php echo base_url('Referral/Home');?>" class="btn btn-primary btn-block">GO HOME!</a>
              
              </h4>
              
                
                </div>
            </div>
        </div>     
    </div>
</div>
</section>