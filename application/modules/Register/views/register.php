<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!------ Include the above in your HEAD tag ---------->
<style>
/* Latest compiled and minified CSS included as External Resource*/

/* Optional theme */

/*@import url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css');*/
body {
    margin-top:30px;
	font-family: Arial, Helvetica, sans-serif;
}
.stepwizard-step p {
    margin-top: 0px;
    color:#666;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 80%;
    position: relative;
    margin: 0 10%;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content:" ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-index: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
fieldset{
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
    padding: 20px 30px;
    box-sizing: border-box;
    width: 80%;
    margin: 0 10%;

    /*stacking fieldsets above each other*/
    position: relative;
}
.fs-title {
    
    text-transform: uppercase;
    color: #2C3E50;
    margin-bottom: 10px;
    letter-spacing: 2px;
    font-weight: bold;
	text-align:center;
}

.fs-subtitle {
    font-weight: normal;
    font-size: 18px;
    color: #666;
    
	text-align:center;
}
small{
	font-weight: normal;
    font-size: 12px;
    color: white;
}
.panel{
	background-color: #6441A5 !important;
}
html {
    height: 100%;
    background: #6441A5; /* fallback for old browsers */
    background: -webkit-linear-gradient(to left, #6441A5, #2a0845); /* Chrome 10-25, Safari 5.1-6 */
}

body {
    font-family: montserrat, arial, verdana;
    background: transparent;
}


.flip-card {
  background-color: transparent;
  width: 300px;
  height: 300px;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.6s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}

.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
}

.flip-card-front, .flip-card-back {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}

.flip-card-front {
  background-color: #bbb;
  color: black;
}

.flip-card-back {
  background-color: #2980b9;
  color: white;
  transform: rotateY(180deg);
}


</style>
<div class="container">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-4"> 
                <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                <p><small>Personal Details</small></p>
            </div>
            <div class="stepwizard-step col-xs-4"> 
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Interest and Qualifications</small></p>
            </div>
            <div class="stepwizard-step col-xs-4"> 
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Profile</small></p>
            </div>
        </div>
    </div>
    
    <form role="form" action="#">
	
        <div class="panel  setup-content" id="step-1">
		<fieldset>
            <div class="panel-heading">
				 <h2 class="fs-title">Personal Details</h2>
                <h3 class="fs-subtitle">Tell us something more about you</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">First Name</label>
                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name</label>
                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" />
                </div>
				<div class="form-group">
                    <label class="control-label">Phone No.</label>
                    <input maxlength="10" minlength="10" type="text" name = "phone" required="required" class="form-control" placeholder="Enter Phone Number" />
                </div>
				<div class="form-group">
                    <label class="control-label">Address</label>
					<textarea maxlength="100" type="text" class="form-control" name="address"  cols="30" rows="2"  required placeholder="Enter Address Name"></textarea>
                </div>
				<div class="form-group">
                    <label class="control-label">City / State</label>
                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter City / State" />
                </div>
				<div class="form-group">
                    <label class="control-label">Country</label>
                    <input maxlength="100" type="text" name="country" required="required" class="form-control" placeholder="Enter Country" />
                </div>
				<div class="form-group">
                    <label class="control-label">Pin Code</label>
                    <input maxlength="6" type="text" name="pincode" required="required" class="form-control" placeholder="Enter Pin Code" />
                </div>
				
				<div class="form-group">
					<label for="sel1">Select Religion:</label>
					<select class="form-control" id="sel1" required >
						<option value="" disabled selected >Select</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
				<div class="form-group">
				<label for="sel1">Select Gender:</label>
					<label class="radio-inline"><input type="radio" name="gender" required>Male</label>
					<label class="radio-inline"><input type="radio" name="gender" required>Female</label>
                </div>
				
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
			</fieldset>
        </div>
	
	
        <div class="panel setup-content" id="step-2">
		<fieldset>
            <div class="panel-heading">
			<h2 class="fs-title">interest and qualifications</h2>
            <h3 class="fs-subtitle">Tell us something more about interest and qualifications</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Height</label>
					<div class="form-group">
				
					<select class="form-control" id="sel1" required >
						<option value="" disabled selected >Select</option>
						<option value="4">4</option>
						<option value="4.5">4.5</option>
						<option value="5">5</option>
						<option value="5.3">5.3</option>
						<option value="5.5">5.5</option>
						<option value="6">6</option>
						<option value="6.2">6.2</option>
						<option value="4">6.5</option>
						<option value="4">7</option>
						<option value="4">7.5</option>
					</select>
				</div>
                </div>
                <div class="form-group">
                    <label class="control-label">Qualification</label>
					<select class="form-control" id="sel1" required >
						<option value="" disabled selected >Select</option>
						<option value="10">10th</option>
						<option value="12">12th</option>
						<option value="bachelor">Bachelor</option>
						<option value="master">Master</option>
					</select>
                </div>
				<div class="form-group">
                    <label class="control-label">Profession</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Profession" />
                </div>

				<div class="form-group">
                    <label class="control-label">Hobbie</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter your Hobbie" />
                </div>
				<div class="form-group">
                    <label class="control-label">Interest</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter your Interest" />
                </div>
				<div class="form-group">
                    <label class="control-label">Mother Tonque</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Mother Tonque" />
                </div>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
			</fieldset>
        </div>
        
        <div class="panel  setup-content" id="step-3">
		<fieldset>
            <div class="panel-heading">
			<h2 class="fs-title">Profile</h2>
            <h3 class="fs-subtitle"></h3>
            </div>
            <div class="panel-body">
				<div class ="form-group">
					<label for="upload photo">Photo</label>
					<input type="file" name = "file" class="form-control">
				</div>
				<!-- <div class="flip-card">
				<div class="flip-card-inner">
					<div class="flip-card-front">
					<img src="" alt="Avatar" style="width:300px;height:300px;">
					</div>
					<div class="flip-card-back">
					<h1>Ravi Bakaria</h1> 
					<p>Architect & Engineer</p> 
					<p>We love that guy</p>
					</div>
				</div>
				</div> -->

				<div class="form-group">
                    <label class="control-label">Date of Birth</label>
                    <input maxlength="200" id = "datepicker" type="text" required="required" class="form-control" placeholder="Enter your Date of Birth" />
                </div>
				<div class="form-group">
                    <label class="control-label">Age</label>
                    <input maxlength="200"  type="text"  class="form-control age" placeholder="" />
                </div>
				<div class="form-group">
                    <label class="control-label">About</label>
					<textarea maxlength="100" type="text" class="form-control abaut" name="description"  cols="30" rows="2"  required placeholder="About"></textarea>
                </div>
                <button class="btn btn-success pull-right" type="submit">Finish!</button>
            </div>
			</fieldset>
        </div>
    </form>
</div>
<script>
$(document).ready(function () {

var navListItems = $('div.setup-panel div a'),
	allWells = $('.setup-content'),
	allNextBtn = $('.nextBtn');

allWells.hide();

navListItems.click(function (e) {
	e.preventDefault();
	var $target = $($(this).attr('href')),
		$item = $(this);

	if (!$item.hasClass('disabled')) {
		navListItems.removeClass('btn-success').addClass('btn-default');
		$item.addClass('btn-success');
		allWells.hide();
		$target.show();
		$target.find('input:eq(0)').focus();
	}
});

allNextBtn.click(function () {
	var curStep = $(this).closest(".setup-content"),
		curStepBtn = curStep.attr("id"),
		nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
		curInputs = curStep.find("input[type='text'],input[type='url'],textarea, select, input[type='radio']"),
		isValid = true;

	// $(".form-group").removeClass("has-error");
	// for (var i = 0; i < curInputs.length; i++) {
	// 	if (!curInputs[i].validity.valid) {
	// 		isValid = false;
	// 		$(curInputs[i]).closest(".form-group").addClass("has-error");
	// 	}
	// }

	if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
});

$('div.setup-panel div a.btn-success').trigger('click');
});
</script>


<script type = "text/javascript" > 
$(document).ready(function(){
	// var date = new Date();
	// var inThreeYears = new Date();
    // var years = inThreeYears.setFullYear (inThreeYears.getFullYear() - 18 );

	// var f = new Date(years);
	// var x=  f.toLocaleDateString('en-GB',{day:"2-digit",month:"2-digit",year:"numeric"})
    
	//console.log(x);
	// $("#datepicker").val(x);
	// $("#datepickerb").focus(function(){
	// 	// alert($(this).val());
	// 	var date1 = new Date($(this).val()); 
	// 	var date2 = new Date(); 
	// 	var Difference_In_Time = date2.getTime() - date1.getTime(); 
	// 	var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
	// 	 console.log(Math.round(Difference_In_Days/365.25)); 
	// 	var age = Math.round(Difference_In_Days/365.25); 
	// 	$(".age").val(age);

	// });

	$( function() {
    $( "#datepicker" ).datepicker({
		changeMonth: true,
        changeYear: true,
		dateFormat: "dd-mm-yy",
		minDate: '-50Y',
    	maxDate: '-18Y'
	});
  } );

});
</script>
