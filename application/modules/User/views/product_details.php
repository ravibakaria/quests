<!-- ============================ ITEM DETAIL ======================== -->
<div class="row " style = "margin:15px" >
		<aside class="col-md-6">
<div class="card">
<article class="gallery-wrap"> 
	<div class="img-big-wrap">
	  <div class = "showimg" style="
    text-align: -webkit-center;
"> <img src="<?php echo base_url('assets/images/'.$images[0]->img_name)?>" class="img-fluid"></div>
	</div> <!-- slider-product.// -->
	<div class="thumbs-wrap">
		<?php 
			if(!empty($images)){
                $i = 1;
                foreach($images as $img){?>
					<a  class="item-thumb getimg" dataid="<?php echo $img->img_name?>"> 
						<img src="<?php echo base_url('assets/images/thumbnails/'.$img->img_name)?>" class="item-thumb">
					</a>
					
					<?php
				} 
			}
		?>
	</div> 
</article> 
</div> 
		</aside>
		<main class="col-md-6">
<article class="product-info-aside">

<h2 class="title mt-3"><?php echo $product->name ;?></h2>

<!-- <div class="rating-wrap my-3">
	<ul class="rating-stars">
		<li style="width:80%" class="stars-active"> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
		<li>
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
	</ul>
	<small class="label-rating text-muted">132 reviews</small>
	<small class="label-rating text-success"> <i class="fa fa-clipboard-check"></i> 154 orders </small>
</div>  -->

<div class="mb-3"> 
	<var class="price h4">&#x20B9; <?php echo $product->pries ;?></var> 
	<span class="text-muted">&#x20B9; <?php echo $product->pries ;?> incl. VAT</span> 
</div> <!-- price-detail-wrap .// -->

<p><?php echo $product->description ;?></p>


<dl class="row">
  <dt class="col-sm-3">Delivery time</dt>
  <?php if($product->delivery !==' '){?>
  <dd class="col-sm-9"><?php echo $product->delivery.' - '.$product->deliveryin;?></dd>
  <?php } ?>
  
  <dt class="col-sm-3">Availabilty</dt>
  <?php if($product->quantity !=''){?>
  <dd class="col-sm-9"><?php echo $product->quantity;?></dd>
  <?php } else{?>
	<dd class="col-sm-9">in Stock</dd> 
  <?php } ?>
<?php if( $product->color !=''){?>
	<dt class="col-sm-3">Colour</dt>
	<dd class="col-sm-9"><?php echo $product->color;?></dd>
<?php } ?>

<?php if($product->size !=''){?>
	<dt class="col-sm-3">Size</dt>
	<dd class="col-sm-9"><?php echo $product->size;?></dd>
<?php } ?>
  <!-- <dt class="col-sm-3">Delivery Charge</dt>
  <?php if($product->deliveryby =='2'){?>
  <dd class="col-sm-9">&#x20B9;<?php echo $product->deliverych;?>.00/-</dd>
  <?php } else{?>
	<button class="btn btn-light" type="button" > Check Delivery Charge</button>
  <?php } ?> -->

</dl>

	<div class="form-row  mt-4">
		<!-- <form method = "post" action="<?= base_url().'User/add_cart'?>"> -->
			
			<input type="hidden" name="id" id = "pruduct_id" value= "<?= $product->id?>">
			
		
		<div class="form-group col-md">
			<div class="input-group mb-3 input-spinner">
			<div class="input-group-append">
			    <button class="btn btn-light" type="button" id="button-minus"> &minus; </button>
			  </div>
			  <input type="text" class="form-control" id = "qty" value="1" name="qty" readonly>
			  <div class="input-group-prepend">
			    <button class="btn btn-light" type="button" id="button-plus"> + </button>
			  </div>
			</div>
		</div> 
		<div class="form-group col-md">
			<button class="btn btn-primary aad_to_cart" type = "submit"> 
				<i class="fa fa-shopping-cart"></i><span class="text">Add to cart</span> 
			</button>
		
		</div> 
	<!-- </form> -->
	</div>
	<?php if($product->parent_id != NULL){ 
		$same = $product->parent_id;
		
		
		$pproduct = $this->db->get_where('products', array('id' => $same))->row();
		$pimages = $this->db->get_where('images', array('product_id' => $pproduct->id))->row();
		
	}else{
		$same = $product->id;
		
		
		$pproduct = $this->db->get_where('products', array('parent_id' => $same))->row();
		if(!empty($pproduct)){ 
		$pimages = $this->db->get_where('images', array('product_id' => $pproduct->id))->row();
		}
		//echo $this->db->last_query();
	}
if(!empty($pproduct) && !empty($pimages)){ ?>


	<div class="form-row  mt-4">
		<div class="form-group col-md">
		
			<?php 
				
			?>
			<div class="form-group col-md">
			<p> Different colours and sizes are available</p>
				<div class="thumbs-wrap">
					<a  class="item-thumb getimg" href ="<?php echo  base_url().'User/product_details/'.$pproduct->id?>"> 
						<img src="<?php echo base_url('assets/images/thumbnails/'.$pimages->img_name)?>" class="item-thumb">
					</a>
				</div>
				
		
			</div>
			<div class="mb-3"> 
				<var class="price">&#x20B9; <?php echo $pproduct->pries ;?></var> 
				<span class="text-muted">&#x20B9; <?php echo $pproduct->pries ;?> incl. VAT</span> 
			</div> <!-- price-detail-wrap .// -->
		</div>
	</div> 
<?php } ?>
</article>
		</main> 
	</div> 

<!-- ================ ITEM DETAIL END .// ================= -->
<!-- =============== SECTION ITEMS =============== -->
<section  class="padding-bottom-sm">

<header class="section-heading heading-line">
	<h4 class="title-section text-uppercase">Recommended items</h4>
</header>

<div class="row row-sm">
	<?php
		foreach($recommendeds as $recommend){?>
         <?php $images = $this->db->get_where('images',['product_id'=>$recommend->id])->row(); ?>
			<div class="col-xl-2 col-lg-3 col-md-4 col-6">
				<div  class="card card-sm card-product-grid">
					<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="img-wrap"> <img src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name);?>"> </a>
					<figcaption class="info-wrap">
						<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="title"><?php echo $recommend->name;?></a>
						<div class="price mt-1">&#x20B9;<?php echo $recommend->pries;?></div> <!-- price-wrap.// -->
					</figcaption>
				</div>
			</div> 
			<?php
		}
	?>
	
</div> <!-- row.// -->
</section>
<!-- =============== SECTION ITEMS .//END =============== -->
<style>
    .btn-primary {
  color: #fff !important;
  background-color: #ff6a00 !important;
  border-color: #ff6a00 !important;
}
</style>

<script>
	$('.getimg').click(function () {
		
		var img = $(this).attr('dataid');
		var src = "<?php echo base_url('assets/images/')?>";
		var html = '<img src="'+src+img+'" class="img-fluid">';
		$(".showimg").html(html);
	});



$(document).ready(function(){
	var count = 1;
	$("#button-plus").click(function() {
		if (count >= 1) {
			++count;
			$('#qty').val(count);
		}
	});
	$("#button-minus").click(function() {
		if (count >= 2) {
			--count;
			$('#qty').val(count);
		}
	});
});

$(document).ready(function(){
    $(".aad_to_cart").click(function(){
       
        var id = $("#pruduct_id").val();
		var qty = $("#qty").val();
        //alert(id);
        $.ajax({ 
            url: "<?= base_url().'User/add_cart'?>",
            data: {'id':id,qty:qty},
            type: 'post',
            success: function(data){
                if(data == "ok"){
					window.location.href = "<?php echo base_url();?>";
				}
                
            }
        });
    });
});
</script>