<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<div class="row">
	<div class ="col-md-8 card mx-auto">
		<div class=" card-body">
			<form method="post" action="<?php echo base_url('Business_partner/add_product_action/'.$product->id);?>" enctype="multipart/form-data">
				<h3>Add Product </h3>
					<div class="form-group">
						<label for="email">Name:</label>
						<input type="text" class="form-control" id="name" value="<?= $product->name;?>" name="name">
						<?php echo form_error('name'); ?>
					</div>

					<div class="form-row">
                <div class="col form-group"> 
                    <label for="varchar">Category </label>
                    <?php echo form_error('service') ?> 
                    <select name="service" class="form-control service" style = "height: 34px" required> 
                        <option value="">Select</option>
                        <?php
                            foreach($services as $value){
                                if($value->id == NULL) {
                                
                                }else{
									if($value->id == $product->service){
										echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
									}else{
										echo "<option  value='".$value->id."'>".$value->name."</option>";
									}
                                    
                                }
                                
                            }
                        ?>
                    </select> 
                </div>
                <div class="col form-group"> 
                    <label for="varchar">Sub Category </label>
                    <?php echo form_error('service1') ?> 
                    <select name="service1" class="form-control service1" style = "height: 34px" required> 
					<option value="">Select</option>
                        <?php
                            foreach($subservices as $value){
                                if($value->id == NULL) {
                                
                                }else{
									if($value->id == $product->subservices){
										echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
									}else{
										echo "<option  value='".$value->id."'>".$value->name."</option>";
									}
                                    
                                }
                                
                            }
                        ?>
                        
                    </select> 
                </div>
            </div>

					<div class="form-row">
						<div class="col form-group">
							<label>Your Selling Price:</label>
							<input type="text" class="form-control" id="pries" name="pries" value="<?= $product->pries;?>">
					<?php echo form_error('pries'); ?>
						</div> 
						<div class="col form-group">
							<label>Your Share [Price - (4% + GST)]</label>
							<input type="text" class="form-control" id="share"  name="share"  value="<?= $product->share;?>" readonly>
						</div> 
            		</div> 
					
					<div class="form-row">
						<div class="col form-group">
							<label>Actual Price/MRP:</label>
							<input type="text" class="form-control" id="showpries" placeholder="Enter Show Price" name="showpries" value="<?php echo $product->showpries ?>">
						</div> 
						<div class="col form-group">
							<label>Quantity:</label>
							<input type="text" class="form-control" id="pries" placeholder="Enter pries" name="quantity" value="<?= $product->quantity;?>">
								<?php echo form_error('quantity'); ?>
						</div> 
					</div> 
					
					<div class="form-row">
					<div class="col form-group">
						<label>Available In Size:</label>
						<input type="text" class="form-control" id="size" placeholder="XXL Or 9-10 Years(single product size)" name="size" >
					</div> 
					<div class="col form-group">
						<label>Available In Colour:</label>
						<input type="text" class="form-control" id="color" placeholder="Enter Colour(single product Colour)" name="color" >
					</div> 
				</div> 
					<div class="form-group">
						<label for="pwd">Description:</label>
						<textarea class="form-control" id="description"  name="description" cols="30" rows="5"><?= $product->description;?></textarea>
						<?php echo form_error('description'); ?>
					</div>
					<div class="form-row">
						<div class="col form-group">
							<label>Delivery :</label>
							<input type="text" class="form-control" id="delivery" placeholder="Enter Show Delivery" name="delivery" value="<?php echo $product->delivery; ?>">
						</div> 
						<div class="col form-group">
							<label>Delivery In:</label>
							<select name="deliveryin" class="form-control deliveryin" style = "height: 34px"> 
								<option value="">Select</option>
								<option <?php if($product->deliveryin =='Minutes'){echo "selected='selected'"; }?>>Minutes</option>
								<option <?php if($product->deliveryin =='Hours'){echo "selected='selected'"; }?>>Hours</option>
								<option <?php if($product->deliveryin =='Days'){echo "selected='selected'"; }?>>Days</option>
								<option <?php if($product->deliveryin =='Working Days'){echo "selected='selected'"; }?>>Working Days</option>
							</select> 
						</div> 
					</div> 
					
					<div class="form-group">
						<label for="">Delivery By:<span class="red-star">*</span></label> 
						<select name="deliveryby" class="form-control deliveryby" style = "height: 34px" required > 
								<option value="">Select</option>
								<option value="1" <?php if($product->deliveryby =='1'){echo "selected='selected'"; }?>>By Admin</option>
								<option value="2" <?php if($product->deliveryby =='2'){echo "selected='selected'"; }?>>By ME</option>
							</select>
					</div> 
					<div class="form-group deliverych">
						<?php if($product->deliveryby =='2'){?>
							<label for="deliverych">Delivery Charge:<span class="red-star">*</span></label> <input type="text" class="form-control" id="deliverych" placeholder="Enter Delivery Charge" name="deliverych" value = "<?php echo $product->deliverych ?>" required >
						<?php } ?>
						
					</div>
					<div class="form-group">
                <label for="">Image:<span class="red-star">*</span></label> 
                <input type="file" class="form-control" id="fname" name="fname[]" required >
            </div> 
            
            <div class="form-group input_fields_wrap">

            </div>
                <button class="add_field_button">Add More</button>
                <?php echo form_error('fname'); ?>
				



				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $(".close").click(function(){
        if(confirm("Are you sure you want to delete this?")){

        }else{
            return false;
        }
        var id = $(this).val();
        //alert(id);
        $.ajax({ 
            url: "<?= base_url('Business_partner/image_delete')?>",
            data: {'id':id},
            type: 'post',
            success: function(data){
                //alert(data);
                location.reload();
            }
        });
    });
});

$(document).ready(function() {
    var max_fields      = 5; //maximum input boxes allowed
    var wrapper       = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="file" class="form-control" id="fname" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

$(document).ready(function(){
  $(".service").change(function(){
    var service = $(".service").val();
    //alert(service);return false;
    $.ajax({
            url: "<?php echo base_url('Product/getsubservice');?>",
            method: "POST",
            data: {service: service},
            cache: true,
            success: function(data)
			{ 
                data = $.parseJSON(data);
                var i;
                var html='';
                for (i = 0; i < data.length; i++) {
                    html += "<option value = '"+data[i].id+"'>"+data[i].name+"</option>"
                }
          
                $(".service1").html(html);
			}
        })
  });
});
$("#pries").keyup(function(){
var p = $("#pries").val();
var m = 4/100;
var g = 18/100;
var tatal = p * m;
var tatal1 = tatal *g;
var main = tatal1 +tatal;
var main1 = (p - main).toFixed(2);

$("#share").val(main1);
});
$('.deliveryby').on('change', function() {
    var by = this.value;
    var html = '<label for="deliverych">Delivery Charge:<span class="red-star">*</span></label> <input type="text" class="form-control" id="deliverych" placeholder="Enter Delivery Charge" name="deliverych"  required >';
    if(by == 2){
		$(".deliverych").html('');
        $(".deliverych").html(html);
    }else{
        $(".deliverych").html('');
    }
    
});
CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>