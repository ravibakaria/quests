
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<style type="text/css">


/* .slick-dots li button {
    font-size: 0;
    line-height: 0;
    
    width: 20px;
    height: 20px;
    padding: 5px;
    cursor: pointer;
    color: transparent;
    border: 0;
    outline: none;
    background: transparent;
} */
/*.slick-prev, .slick-next {
    font-size: 0;
    line-height: 0;
    position: absolute;
    top: 50%;
    display: block;
    width: 20px;
    height: 20px;
    padding: 0;
    -webkit-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
    cursor: pointer;
    color: transparent;
    border: none;
    outline: none;
    background: transparent;
    }*/
    

.slick-track {
    position: relative;
    top: 0;
    left: 0;
    display: block;
    margin-left: auto;
    margin-right: auto;
   
}
.draggable{
    opacity: 1;
    width: 550px !important;
    height: 400px !important;
    margin-bottom: 21px;
}
/* .slick-slide{
    width: 150px !important;
    height: 150px !important;
} */
.slider-nav{
    width: 525px !important;
    height: 170px !important;
}
.slider-for{
    width: 525px !important;
    height: 400px !important;
    margin-bottom: 21px;
}
.slick-dots{
	display: none !important;
}
.slick-prev {
	display: none !important;
}
.slick-next {
	display: none !important;
}
.page-wrapper .page-content {
    overflow-x: unset !important;
} 
</style>

<div class="container">
    <div class = "row">
    <div class="col-md-6">
	<div class="slider slider-for ">
	    <?php foreach($images as $image){?>
		    <div>
                <a href="<?php echo base_url('assets/images/'.$image->img_name)?>" data-fancybox="images">
                    <img src="<?php echo base_url('assets/images/'.$image->img_name)?>" width = "400" height = "400px"  style="    margin-left: 18%;">
                </a>
		    </div>
	    <?php } ?>
	</div>
	<div class="slider slider-nav">
        <?php foreach($images as $image){?>
            <div>
                <img src="<?= base_url().'assets/images/thumbnails/'.$image->img_name?>" width = "120" height = "120">
            </div>
        <?php } ?>
	</div>
</div>
<div class="col-md-6 text-right">
<h1><span> Name = <?php echo $product->name;?> </span></h1>
<h3><span> Prices = <?php echo $product->pries;?> </span></h3>
<h5><span> Gst = <?php echo $product->gst;?> %</span></h5>
<p></b>Status = </b><?php if($product->status ==0){echo "InActive";}?></p>
<p><?php echo $product->description;?></p>
<form method = "post" action="<?= base_url().'Product/active'?>">
   
    <input type="hidden" name="id" value= "<?= $product->id?>">
    <br>
    <input type = "submit" class="btn btn-warning" value = "Active">
</form>
</div>
</div>

</div>

<script type="text/javascript">
var $j = jQuery.noConflict();
$j(document).ready(function(){
    
    


$j('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$j('.slider-nav').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});


});
</script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

