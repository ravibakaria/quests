
<?php 
    if($this->session->userdata('user')->status == 0){
        $status = "InActive";
    }
?>
<header id="main-header" class="py-2 bg-danger text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 style="text-transform: capitalize">
                    <i class="fa fa-users"></i>  Status: <?php echo $status; ?>
                </h1>
            </div>
        </div>
    </div>
</header>

<section class="section-content padding-y">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card text-center bg-danger text-white mb-3">
                <div class="card-body">
                <h4 class="display-4">
                    Please Activate Your Account <br> By Paying &#8377 <?php echo $amt->gst;?>
                </h4>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card text-center bg-danger text-white mb-3">
                <div class="card-body">
                <h4 class="display-4">
                <!-- PAYMENT FORM/// -->
                    <button  class="btn btn-warning btn-block" onclick="razorpaySubmit(this);">Pay now!</button>
                <!-- PAYMENT FORM/// -->
                </h4>
                </div>
            </div>
        </div>     
    </div>
</div>
</section>
<?php


$txnid = time();        
$key_id = RAZOR_KEY_ID;
$currency_code = 'INR';            
$total =  $amt->gst * 100; 
$amount = $amt->gst; 
$merchant_order_id =  rand(10000,99999);

$email = 'info@techarise.com';
$phone = '9000000001';
$name = APPLICATION_NAME;
$return_url = base_url().'Business_partner/callback';
$surl = base_url().'Business_partner/success';
$furl = base_url().'Business_partner/failed';
?>
 <form name="razorpay-form" id="razorpay-form" action="<?php echo $return_url; ?>" method="POST">
  <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
  <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id; ?>"/>
  <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
 
  <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
  <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
  <!-- <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/> -->
  <input type="hidden" name="merchant_total" id="merchant_total" value="<?php echo $total; ?>"/>
  <input type="hidden" name="merchant_amount" id="merchant_amount" value="<?php echo $amount; ?>"/>
</form>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var razorpay_options = {
    key: "<?php echo $key_id; ?>",
    amount: "<?php echo $total; ?>",
    name: "<?php echo $name; ?>",
    description: "Order # <?php echo $merchant_order_id; ?>",
    netbanking: true,
    currency: "<?php echo $currency_code; ?>",
    prefill: {
      name:"",
      email: "",
      contact: ""
    },
    notes: {
      soolegal_order_id: "<?php echo $merchant_order_id; ?>",
    },
    handler: function (transaction) {
        document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
        document.getElementById('razorpay-form').submit();
    },
    "modal": {
        "ondismiss": function(){
            location.reload()
        }
    }
  };
  var razorpay_submit_btn, razorpay_instance;

  function razorpaySubmit(el){
    if(typeof Razorpay == 'undefined'){
      setTimeout(razorpaySubmit, 200);
      if(!razorpay_submit_btn && el){
        razorpay_submit_btn = el;
        el.disabled = true;
        el.value = 'Please wait...';  
      }
    } else {
      if(!razorpay_instance){
        razorpay_instance = new Razorpay(razorpay_options);
        if(razorpay_submit_btn){
          razorpay_submit_btn.disabled = false;
          razorpay_submit_btn.value = "Pay Now";
        }
      }
      razorpay_instance.open();
    }
  }  
</script>