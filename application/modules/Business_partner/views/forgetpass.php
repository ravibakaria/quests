<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<div class="jumbotron " style="
    background-color: black;border-radius:unset
">
  			<div class="widget-header mr-3" style=" height: 76px;">
				<a href="https://quests.co.in/" class="brand-wrap">
					<img class="logo" src="<?php echo base_url('assets/')?>images/BUSINESS.png" style="height: 200px;margin-top: -62px;"> 
				</a> <!-- brand-wrap.// -->
				</div>
    <h1 class = "text-center" style="
    margin-top: -61px; color: white;
">Business Partner</h1>      
  </div>

<style>

</style>
<?php if($this->session->flashdata('message')){?>
        <div class="alert alert-success">
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('message_r')){?>
        <div class="alert alert-warning">
            <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
<div class="container padding-bottom-3x mb-2 mt-5">
	    <div class="row justify-content-center">
	        <div class="col-lg-8 col-md-10">
	            <div class="forgot">
	                <h2>Forgot your password?</h2>
	                <p>Change your password in three easy steps. This will help you to secure your password!</p>
	                <ol class="list-unstyled">
	                    <li><span class="text-primary text-medium">1. </span>Enter your email address below.</li>
	                    <li><span class="text-primary text-medium">2. </span>Our system will send you a temporary link</li>
	                    <li><span class="text-primary text-medium">3. </span>Use the link to reset your password</li>
	                </ol>
	            </div>
	            <form class="card mt-4"  method = "post" action = "<?php echo base_url('Business_partner/forgetpassaction')?>">
	                <div class="card-body">
	                    <div class="form-group"> <label for="email-for-pass">Enter your email address</label> <input class="form-control" type="email" id="email-for-pass" name = 'email' required=""><small class="form-text text-muted">Enter the email address you used during the registration on <?php echo base_url();?> Then we'll email a link to this address.</small> </div>
	                </div>
	                <div class="card-footer"> <button class="btn btn-success" type="submit">Get New Password</button> <a href = "<?php echo base_url('Business_partner');?>" class="btn btn-danger" type="submit">Back to Login</a> </div>
	            </form>
	        </div>
	    </div>
	</div>