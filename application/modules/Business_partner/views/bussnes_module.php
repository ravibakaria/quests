<section class="section-content padding-y">
    <div class="card mx-auto" style="max-width:720px; margin-top:40px;">
        <article class="card-body">
		    <header class="mb-4"><h4 class="card-title">My Business Modules</h4></header>
            <?php if($this->session->flashdata('message')){?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('message_r')){?>
                <div class="alert alert-warning">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
                </div>
                <?php } ?>
            <form id = "registerform" method = "post" action = "<?php echo base_url('Business_partner/busniess_update_action');?>">
                <div class="form-group">
					<label>Businss Type</label>
					<select name="bussines" class="form-control bussines" style = "height: 34px"> 
                    <option value="">Select</option>
                    <?php
                    foreach($services as $value)
                    {
                        echo "<option value='".$value->id."'>".$value->name."</option>";
                    }
                    ?>
                </select> 
				</div> <!--form-group end.//-->
                
                <p class="font-weight-bold">Choose Business Modules</p>
                    <?php echo form_error('access') ?> </label>
                <ul class="list-group list-group-flush">
                    
                       
                </ul>
                <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Update  </button>
			    </div> <!-- form-group// --> 
            </form>
        </article>
    </div>
</section>
<script>
$(document).on('change','.bussines',function(){
    var bc = $(this).val();
    $.ajax({
        url: "<?php echo base_url('Business_partner/select_bussnise_modules1');?>",
        method: "POST",
        data: {bc: bc},
        cache: true,
        success: function(data)
        { 
            data = $.parseJSON(data);
            var i;
            var html='';
            var j =1;
            for (i = 0; i < data.length; i++) {
                j++;
                html += "<li class='list-group-item'><div class='custom-control custom-radio'><input type='radio' class='custom-control-input' id='check"+ j +"' name = 'bmodules' value='"+data[i].id+"'  ><label class='custom-control-label' for='check"+j+"'>"+data[i].name+"</label> <p>"+data[i].description+"</p></div></li>";
               
            }
        
            $(".list-group-flush").html(html);
        }
    })
})
</script>