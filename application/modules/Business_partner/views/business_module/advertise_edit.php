<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<div class="row">
    <div class ="col-md-6 card mx-auto ">

        <div class=" card-body">
            <h2>Edit Advertise</h2>
            <div style = "text-align: right";>
                <a href="<?= base_url().'Business_partner/advertise';?>" class="btn btn-primary">List Of Advertise</a>
            </div>
            <form action="<?= base_url('Business_partner/update_advertise_action/'.$advertise->id)?>" method = "POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="email">Advertise Name:</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo $advertise->name; ?>">
                    <?php echo form_error('name'); ?>
                </div>
                <div class="form-group">
                    <label for="pwd">Description:</label>
                    <textarea name="description" class="form-control" id="description" cols="30" rows="5"><?php echo $advertise->discription; ?></textarea>
                    
                    <?php echo form_error('description'); ?>
                </div>
                <?php if($advertise->img_name == ''){?>
                    <div class="form-group">
                        <div class="custom-file">
                            <input name="fname" type="file" class="custom-file-input" aria-describedby="">
                            <label class="custom-file-label" for="">Add Image</label>
                        </div>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <img src="<?php echo base_url('assets/images/'.$advertise->img_name)?>" alt="" srcset="" height = "200px" width = "500px">
                    </div>
                <?php } ?>
                <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Save  </button>
			    </div> <!-- form-group// -->  
            </form>
        </div>
    </div>
</div>
<script>
CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>