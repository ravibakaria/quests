<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<div class="row">
	<div class ="col-md-8 card mx-auto">
		<div class=" card-body">
			<form method="post" action="<?php echo base_url('Business_partner/update_Services/'.$Services->id);?>" enctype="multipart/form-data">
				<h3>Update Services </h3>
					<div class="form-group">
						<label for="email"> Services Name:</label>
						<input type="text" class="form-control" id="name" value="<?= $Services->name;?>" name="name">
						<?php echo form_error('name'); ?>
					</div>
					<div class="form-row">
						<div class="col form-group">
							<label>Actual Price:</label>
							<input type="text" class="form-control" id="pries" value="<?= $Services->pries;?>" name="pries">
					<?php echo form_error('pries'); ?>
						</div> 
						<div class="col form-group">
							<label>Your Share [Price - (4% + GST)]</label>
							<input type="text" class="form-control" id="share"  name="share"  value="<?= $Services->share;?>" readonly>
						</div> 
            		</div> 
					
					<div class="form-row">
						<div class="col form-group">
							<label>Show Price:</label>
							<input type="text" class="form-control" id="showpries" placeholder="Enter Show Price" name="showpries" value="<?php echo $Services->showpries ?>">
						</div> 
						<div class="col form-group">
                            <label>Pack Type:</label>
                            <select name="packtype" class="form-control packtype" style = "height: 34px"> 
                                <option value="">Select</option>
                                <option <?php if($Services->packtype =='1 Month'){echo "selected='selected'"; }?>>1 Month </option>
                                <option <?php if($Services->packtype =='3 Month'){echo "selected='selected'"; }?>>3 Month </option>
                                <option <?php if($Services->packtype =='6 Month'){echo "selected='selected'"; }?>>6 Month </option>
                                <option <?php if($Services->packtype =='Yearly'){echo "selected='selected'"; }?>> Yearly </option>
                            </select> 
                        </div> 
					</div> 

                    <div class="form-group">
                        <label for="pwd">How Many Times Service:</label>
                        <select name="timesservice" class="form-control timesservice" style = "height: 34px"> 
                            <option value="">Select</option>
                            <option <?php if($Services->timesservice =='1'){echo "selected='selected'"; }?>>1</option>
                            <option <?php if($Services->timesservice =='2'){echo "selected='selected'"; }?>>2</option>
                            <option <?php if($Services->timesservice =='3'){echo "selected='selected'"; }?>>3</option>
                            <option <?php if($Services->timesservice =='4'){echo "selected='selected'"; }?>>4</option>
                            <option <?php if($Services->timesservice =='5'){echo "selected='selected'"; }?>>5</option>
                            <option <?php if($Services->timesservice =='6'){echo "selected='selected'"; }?>>6</option>
                            <option <?php if($Services->timesservice =='7'){echo "selected='selected'"; }?>>7</option>
                            <option <?php if($Services->timesservice =='8'){echo "selected='selected'"; }?>>8</option>
                            <option <?php if($Services->timesservice =='9'){echo "selected='selected'"; }?>>9</option>
                            <option <?php if($Services->timesservice =='10'){echo "selected='selected'"; }?>>10</option>
                            <option <?php if($Services->timesservice =='11'){echo "selected='selected'"; }?>>11</option>
                            <option <?php if($Services->timesservice =='12'){echo "selected='selected'"; }?>>12</option>
                            </select> 
                        <?php echo form_error('timesservice'); ?>
                    </div>

					<div class="form-group">
						<label for="pwd">Description:</label>
                        <textarea name="description" id="description" class="form-control" cols="30" rows="5"><?php echo $Services->discription ?></textarea>
						<?php echo form_error('description'); ?>
					</div>
					
					<div class="form-group"> 
						<label for="varchar">Service</label>  
						<?php echo form_error('service') ?> 
						<select name="service" class="form-control service" style = "height: 34px"> 
							<option value="">Select</option>
							<?php
								foreach($services as $value){
									if($value->id == $Services->service){
										echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
									}else{
										if($value->id == $this->session->userdata('user')->bussines_type1){
											echo "<option  value='".$value->id."'>".$value->name."</option>";
										}
										if($value->id == $this->session->userdata('user')->bussines_type2){
											echo "<option  value='".$value->id."'>".$value->name."</option>";
										}
									}
								}
							?>
						</select> 
					</div> 

					<?php $images = $this->db->get_where('service_contract_img',['service_contract_id'=>$Services->id])->result();
						if(count($images)>=5){

						}else{?>
						<div class="form-group">
							<label for="pwd">Image:</label> 
								<input type="file" class="form-control" id="fname" name="fname[]"> 
							<div class="input_fields_wrap">

							</div>
							<button class="add_field_button">Add More</button>

						</div>
					<?php } ?>
				<div class="row">

					<?php foreach ($images as $key => $image) {?>
						<div class="col-md-3">
							<button type="button" class="close" value="<?= $image->id?>">&times;</button>
							<img src="<?= base_url().'assets/images/thumbnails/'.$image->img_name?>">
						</div>
					<?php } ?>
				</div>



				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$(".close").click(function(){
		if(confirm("Are you sure you want to delete this?")){

		}
		else{
		return false;
		}
		var id = $(this).val();
		//alert(id);
		$.ajax({ 
			url: "<?= base_url('Business_partner/image_delete')?>",
			data: {'id':id},
			type: 'post',
			success: function(data){
				//alert(data);
				location.reload();
			}
		});
	});
});

$(document).ready(function() {
var max_fields      = <?php echo 5 - count($images);?>; //maximum input boxes allowed
var wrapper       = $(".input_fields_wrap"); //Fields wrapper
var add_button      = $(".add_field_button"); //Add button ID

var x = 1; //initlal text box count
$(add_button).click(function(e){ //on add input button click
e.preventDefault();
if(x < max_fields){ //max input box allowed
x++; //text box increment
$(wrapper).append('<div><input type="file" class="form-control" id="fname" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
}
});

$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
e.preventDefault(); $(this).parent('div').remove(); x--;
})
});


$("#pries").keyup(function(){
var p = $("#pries").val();
var m = 4/100;
var g = 18/100;
var tatal = p * m;
var tatal1 = tatal *g;
var main = tatal1 +tatal;
var main1 = (p - main).toFixed(2);

$("#share").val(main1);
});

CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>