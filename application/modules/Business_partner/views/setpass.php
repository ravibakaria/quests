<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<div class="jumbotron " style="
    background-color: black;border-radius:unset
">
  			<div class="widget-header mr-3" style=" height: 76px;">
				<a href="https://quests.co.in/" class="brand-wrap">
					<img class="logo" src="<?php echo base_url('assets/')?>images/BUSINESS.png" style="height: 200px;margin-top: -62px;"> 
				</a> <!-- brand-wrap.// -->
				</div>
    <h1 class = "text-center" style="
    margin-top: -61px; color: white;
">Business Partner</h1>      
  </div>

<style>

</style>
<?php if($this->session->flashdata('message')){?>
        <div class="alert alert-success">
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('message_r')){?>
        <div class="alert alert-warning">
            <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
<div class="container padding-bottom-3x mb-2 mt-5">
	    <div class="row justify-content-center">
	        <div class="col-lg-8 col-md-10">
	            <div class="forgot">
	                <h2>Change your password.</h2>
	                
	            </div>
	            <form class="card mt-4 loginform"  method = "post" action = "<?php echo base_url('Business_partner/setpassaction')?>">
	                <div class="card-body">
	                    <div class="form-group"> <label for="email-for-pass">Enter New Password</label> <input class="form-control" type="password" id="password" name = 'password' required="">
                        <div class="form-group"> <label for="email-for-pass">Re-Enter  Password</label> <input class="form-control" type="password" id="email-for-pass" name = 'cpassword' required=""> </div>
	                </div>
                    <input class="form-control" type="hidden" id="email-for-pass" name = 'token' value = '<?php echo $token?>'>
	                <div class="card-footer"> <button class="btn btn-success" type="submit">Submit</button> 
	            </form>
	        </div>
	    </div>
	</div>

 
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
    $j(".loginform").validate({
        rules: {
            
            password: {
                required: true,
                minlength: 6
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
			
        },
        messages: {
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            }
        }
    });
});
</script>