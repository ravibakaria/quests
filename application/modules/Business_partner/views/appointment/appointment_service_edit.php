<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<div class="row">
	<div class ="col-md-8 card mx-auto">
		<div class=" card-body">
			<form method="post" action="<?php echo base_url('Business_partner/update_ap_Services/'.$Services->id);?>" enctype="multipart/form-data">
				<h3>Update Services </h3>
					<div class="form-group">
						<label for="email"> Services Name:</label>
						<input type="text" class="form-control" id="name" value="<?= $Services->name;?>" name="name">
						<?php echo form_error('name'); ?>
					</div>
					<div class="form-row">
						<div class="col form-group">
							<label>Actual Price:</label>
							<input type="text" class="form-control" id="pries" value="<?= $Services->pries;?>" name="pries">
					<?php echo form_error('pries'); ?>
						</div> 
						<div class="col form-group">
							<label>Your Share [Price - (4% + GST)]</label>
							<input type="text" class="form-control" id="share"  name="share"  value="<?= $Services->share;?>" readonly>
						</div> 
            		</div> 
					
					<div class="form-row">
						<div class=" col form-group">
							<label>Show Price:</label>
							<input type="text" class="form-control" id="showpries" placeholder="Enter Show Price" name="showpries" value="<?php echo $Services->showpries ?>">
						</div> 
						<div class=" col form-group">
						<div class="">
							<label>Price On Per:</label>
							<select name="priceonper" id="" class="form-control">
							<option value="">Select</option>
							<option value="Hours" <?php if($Services->priceonper == 'Hours'){echo "selected='selected'";} ?>>-/Hours</option>
							<option value="Square Feet" <?php if($Services->priceonper == 'Square Feet'){echo "selected='selected'";} ?>>-/Square Feet</option>
							<option value=""></option>
							<option value=""></option>
							<option value=""></option>
							</select>
						</div> 
					</div> 
                    
                </div>
					

                    

					<div class="form-group">
						<label for="pwd">Description:</label>
                        <textarea name="description" id="description" class="form-control" cols="30" rows="5"><?php echo $Services->discription ?></textarea>
						<?php echo form_error('description'); ?>
					</div>
					
					<div class="form-group"> 
						<label for="varchar">Service</label>  
						<?php echo form_error('service') ?> 
						<select name="service" class="form-control service" style = "height: 34px"> 
							<option value="">Select</option>
							<?php
								foreach($services as $value){
									if($value->id == $Services->service_id){
										echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
									}else{
                                        if($value->id == $this->session->userdata('user')->bussines_type1){
                                            echo "<option  value='".$value->id."'>".$value->name."</option>";
                                        }
                                        if($value->id == $this->session->userdata('user')->bussines_type2){
                                            echo "<option  value='".$value->id."'>".$value->name."</option>";
                                        }
                                    }
								}
							?>
						</select> 
					</div> 

					<?php if ($Services->img_name !== '') {?>
                            <div class="col-md-3">
                                <button type="button" class="close" value="<?= $Services->id?>">&times;</button>
                                <img src="<?= base_url().'assets/images/'.$Services->img_name?>" height = "200px" width = "200px" class = "img img-thumbnail">
						    </div>
						<?php }else{?>
						    <div class="form-group"> 
							    <label for="pwd">Image:</label> 
								<input type="file" class="form-control" id="fname" name="fname"> 
						</div>
					<?php } ?>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
$(".close").click(function(){
if(confirm("Are you sure you want to delete this?")){

}
else{
return false;
}
var id = $(this).val();
//alert(id);
$.ajax({ 
url: "<?= base_url('Business_partner/image_delete_ap_service')?>",
data: {'id':id},
type: 'post',
success: function(data){
//alert(data);
location.reload();
}
});
});
});




$("#pries").keyup(function(){
var p = $("#pries").val();
var m = 4/100;
var g = 18/100;
var tatal = p * m;
var tatal1 = tatal *g;
var main = tatal1 +tatal;
var main1 = (p - main).toFixed(2);

$("#share").val(main1);
});


CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });

</script>