<style>
fieldset {
    display: block;
    margin-inline-start: 2px;
    margin-inline-end: 2px;
    padding-block-start: 0.35em;
    padding-inline-start: 0.75em;
    padding-inline-end: 0.75em;
    padding-block-end: 0.625em;
    min-inline-size: min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
</style>
<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<div class="row">
    <div class ="col-md-6 card mx-auto ">

        <div class=" card-body">
            <h2>Edit Portfolio</h2>
            <div style = "text-align: right";>
                <a href="<?= base_url().'Business_partner/portfolio';?>" class="btn btn-primary">Portfolio</a>
            </div>
            <form action="<?= base_url('Business_partner/update_portfolio_action')?>" method = "POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="email">Name:</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo ucfirst($this->session->userdata('user')->shopname)?>" readonly>
                    <?php echo form_error('name'); ?>
                </div>
                <div class="form-group">
                    <label for="pwd">Description:</label>
                    <textarea name="description" class="form-control" id="editor1" cols="30" rows="5"><?php echo  $advertise->discription; ?></textarea>
                </div>
                <?php  if($advertise->opendays ==''){
								$opendays = [];
							}else{
								$opendays = explode(" ",$advertise->opendays);
								$opendays = explode((','),$opendays[0]);
							}
					?>
                <div class="form-group">
                    <label>Shop Opne Days :</label>
                    <select name="weekdays[]" class="form-control weekdays" multiple > 
                        <option value="">All</option>
                        <option <?php if(in_array("Monday", $opendays)){echo "selected='selected'"; }?>>Monday</option>
                        <option <?php if(in_array("Tuesday", $opendays)){echo "selected='selected'"; }?>>Tuesday</option>
                        <option <?php if(in_array("Wednesday", $opendays)){echo "selected='selected'"; }?>>Wednesday</option>
                        <option <?php if(in_array("Thursday", $opendays)){echo "selected='selected'"; }?>>Thursday</option>
                        <option <?php if(in_array("Friday", $opendays)){echo "selected='selected'"; }?>>Friday</option>
                        <option <?php if(in_array("Saturday", $opendays)){echo "selected='selected'"; }?>>Saturday</option>
                        <option <?php if(in_array("Sunday", $opendays)){echo "selected='selected'"; }?>>Sunday</option>
                       
                        
                    </select> 
                </div> 
                <fieldset>
  					<legend>Shop Opne Time:</legend>
                <div class="form-row">
                    <div class="col form-group">
                        <label>Morning:</label>
                        <select name="morning_time" class="form-control aday" style = "height: 34px"> 
                        <option value="">Select</option>
                        <option value="m4" <?php if($advertise->morning_time =='m4'){echo "selected='selected'"; }?>>8:00 Am To 12:00 PM</option>
                        <option value="m3" <?php if($advertise->morning_time =='m3'){echo "selected='selected'"; }?>>9:00 Am To 12:00 PM</option>
                        <option value="m2" <?php if($advertise->morning_time =='m2'){echo "selected='selected'"; }?>>10:00 Am To 12:00 PM</option>
                        <option value="m1" <?php if($advertise->morning_time =='m1'){echo "selected='selected'"; }?>>11:00 Am To 12:00 PM</option>
                       
                        
                    </select> 
                    </div> 
                    <div class="col form-group">
                        <label>Booking Allow:</label>
                        <select name="morning_booking" class="form-control aday" style = "height: 34px"> 
                        <option value="">Select</option>
                        <option <?php if($advertise->morning_booking =='1'){echo "selected='selected'"; }?>>1</option>
                        <option <?php if($advertise->morning_booking =='2'){echo "selected='selected'"; }?>>2</option>
                        <option <?php if($advertise->morning_booking =='3'){echo "selected='selected'"; }?>>3</option>
                        <option <?php if($advertise->morning_booking =='4'){echo "selected='selected'"; }?>>4</option>
                        <option <?php if($advertise->morning_booking =='5'){echo "selected='selected'"; }?>>5</option>
                        <option <?php if($advertise->morning_booking =='6'){echo "selected='selected'"; }?>>6</option>
                        <option <?php if($advertise->morning_booking =='7'){echo "selected='selected'"; }?>>7</option>
                        <option <?php if($advertise->morning_booking =='8'){echo "selected='selected'"; }?>>8</option>
                        <option <?php if($advertise->morning_booking =='9'){echo "selected='selected'"; }?>>9</option>
                        <option <?php if($advertise->morning_booking =='10'){echo "selected='selected'"; }?>>10</option>
                        </select>
                    </div>
                    
                
                </div> 
                <div class="form-row">
                    <div class="col form-group">
                        <label>After Noon:</label>
                        <select name="after_time" class="form-control aday" style = "height: 34px"> 
                        <option value="">Select</option>
                        <option value="a4" <?php if($advertise->after_time =='a4'){echo "selected='selected'"; }?>>1:00 PM To 4:00 PM</option>
                        <option value="a5" <?php if($advertise->after_time =='a5'){echo "selected='selected'"; }?>>1:00 PM To 5:00 PM</option>
                        <option value="a2" <?php if($advertise->after_time =='a2'){echo "selected='selected'"; }?>>2:00 PM To 4:00 PM</option>
                        <option value="a3" <?php if($advertise->after_time =='a3'){echo "selected='selected'"; }?>>2:00 AP To 5:00 PM</option>
                       
                        
                    </select> 
                    </div> 
                    <div class="col form-group">
                    <label>Booking Allow:</label>
                        <select name="after_booking" class="form-control aday" style = "height: 34px"> 
                        <option value="">Select</option>
                        <option <?php if($advertise->after_booking =='1'){echo "selected='selected'"; }?>>1</option>
                        <option <?php if($advertise->after_booking =='2'){echo "selected='selected'"; }?>>2</option>
                        <option <?php if($advertise->after_booking =='3'){echo "selected='selected'"; }?>>3</option>
                        <option <?php if($advertise->after_booking =='4'){echo "selected='selected'"; }?>>4</option>
                        <option <?php if($advertise->after_booking =='5'){echo "selected='selected'"; }?>>5</option>
                        <option <?php if($advertise->after_booking =='6'){echo "selected='selected'"; }?>>6</option>
                        <option <?php if($advertise->after_booking =='7'){echo "selected='selected'"; }?>>7</option>
                        <option <?php if($advertise->after_booking =='8'){echo "selected='selected'"; }?>>8</option>
                        <option <?php if($advertise->after_booking =='9'){echo "selected='selected'"; }?>>9</option>
                        <option <?php if($advertise->after_booking =='10'){echo "selected='selected'"; }?>>10</option>
                        </select>
                    </div>
                    
                
                </div> 
                <div class="form-row">
                    <div class="col form-group">
                        <label>Evening:</label>
                        <select name="evning_time" class="form-control aday" style = "height: 34px"> 
                        <option value="">Select</option>
                        <option value="e4" <?php if($advertise->evning_time =='e4'){echo "selected='selected'"; }?>>4:00 PM To 8:00 PM</option>
                        <option value="e5" <?php if($advertise->evning_time =='e5'){echo "selected='selected'"; }?>>4:00 PM To 9:00 PM</option>
                        <option value="e6" <?php if($advertise->evning_time =='e6'){echo "selected='selected'"; }?>>4:00 PM To 10:00 PM</option>
                        <option value="e3" <?php if($advertise->evning_time =='e3'){echo "selected='selected'"; }?>>5:00 PM To 8:00 PM</option>
                        <option value="ee4" <?php if($advertise->evning_time =='ee4'){echo "selected='selected'"; }?>>5:00 PM To 9:00 PM</option>
                        <option value="ee5" <?php if($advertise->evning_time =='ee5'){echo "selected='selected'"; }?>>5:00 PM To 10:00 PM</option>
                        <option value="e2" <?php if($advertise->evning_time =='e2'){echo "selected='selected'"; }?>>6:00 PM To 8:00 PM</option>
                        <option value="ee3" <?php if($advertise->evning_time =='ee3'){echo "selected='selected'"; }?>>6:00 PM To 9:00 PM</option>
                        <option value="eee4" <?php if($advertise->evning_time =='eee4'){echo "selected='selected'"; }?>>6:00 PM To 10:00 PM</option>
                       
                        
                    </select> 
                    </div> 
                    <div class="col form-group">
                    <label>Booking Allow:</label>
                        <select name="evning_booking" class="form-control aday" style = "height: 34px"> 
                        <option value="">Select</option>
                        <option <?php if($advertise->evning_booking =='1'){echo "selected='selected'"; }?>>1</option>
                        <option <?php if($advertise->evning_booking =='2'){echo "selected='selected'"; }?>>2</option>
                        <option <?php if($advertise->evning_booking =='3'){echo "selected='selected'"; }?>>3</option>
                        <option <?php if($advertise->evning_booking =='4'){echo "selected='selected'"; }?>>4</option>
                        <option <?php if($advertise->evning_booking =='5'){echo "selected='selected'"; }?>>5</option>
                        <option <?php if($advertise->evning_booking =='6'){echo "selected='selected'"; }?>>6</option>
                        <option <?php if($advertise->evning_booking =='7'){echo "selected='selected'"; }?>>7</option>
                        <option <?php if($advertise->evning_booking =='8'){echo "selected='selected'"; }?>>8</option>
                        <option <?php if($advertise->evning_booking =='9'){echo "selected='selected'"; }?>>9</option>
                        <option <?php if($advertise->evning_booking =='10'){echo "selected='selected'"; }?>>10</option>
                        </select>
                    </div>
                    
                
                </div> 
            </fieldset>
                
            
                <div class="form-group">
                    <label for="email">facebook Link :</label>
                    <input type="text" class="form-control" id="fb"  name="fb" value="<?php echo  $advertise->fb; ?>" >
                </div>
                <div class="form-group">
                    <label for="email">Instagram Link:</label>
                    <input type="text" class="form-control" id="insta" name="insta" value="<?php echo  $advertise->insta; ?>">
                   
                </div>
                <div class="form-group">
                    <label for="email">YouTube Link:</label>
                    <input type="text" class="form-control" id="youtube"  name="youtube" value="<?php echo  $advertise->youtube; ?>">
                   
                </div>
                <?php if($advertise->img_name == ''){?>
                    <div class="form-group">
                        <div class="custom-file">
                            <input name="fname" type="file" class="custom-file-input" aria-describedby="">
                            <label class="custom-file-label" for="">Add Image</label>
                        </div>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                    <button type="button" class="close" value="<?= $advertise->id?>">&times;</button>
                        <img src="<?php echo base_url('assets/images/'.$advertise->img_name)?>" alt="" srcset="" height = "200px" width = "500px">
                    </div>
                <?php } ?>
                <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Save  </button>
			    </div> <!-- form-group// -->  
            </form>
        </div>
    </div>
</div>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".close").click(function(){
        if(confirm("Are you sure you want to delete this?")){

        }else{
            return false;
        }
        var id = $(this).val();
        //alert(id);
        $.ajax({ 
            url: "<?= base_url('Business_partner/portfolioimg')?>",
            data: {'id':id},
            type: 'post',
            success: function(data){
                //alert(data);
                location.reload();
            }
        });
    });
});

CKEDITOR.replace('editor1', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });

    $(".weekdays").select2();
    $('.weekdays').on("select2:select", function (e) { 
           var data = e.params.data.text;
           if(data=='All'){
            $(".weekdays > option").prop("selected","selected");
            $(".weekdays").trigger("change");
           }
      });
</script>
