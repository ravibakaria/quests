
<style>
.card-mini .card-body h2 {
    color: rgb(27, 34, 60);
    font-size: 1.49625rem;
}

.mb-1, .my-1 {
    margin-bottom: 0.25rem !important;
}

.card-default .card-header {
    padding-left: 1.88rem;
    padding-right: 1.88rem;
    padding-top: 3rem;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    background-color: #ffffff;
    border-bottom: none;
}
.card-header:first-child {
    border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
.card-table-border-none .card-header {
    padding-left: 1.88rem;
    padding-right: 1.88rem;
    padding-top: 3rem;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    background-color: #ffffff;
    border-bottom: none;
}
.justify-content-between {
    justify-content: space-between !important;
}
.card-table-border-none .card-body {
    padding: 1.9rem 1.88rem;
}
.table-responsive {
    display: table;
    overflow-x: auto;
}
.card-table {
    margin-top: 3.12rem;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #8a909d;
}
.date-range-report span:after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 1em;
    content: "";
    border-right: 5px solid transparent;
    border-bottom: 0px;
    border-top: 6px solid;
    border-left: 5px solid transparent;
    vertical-align: middle;
    
}
.dropdown-toggle::after {
    display: none;
}
@media screen and (min-width: 1200px) {
    #linechart {
    min-height: 280px !important;
}
  }
  


/* .widget-dropdown .dropdown-toggle.icon-burger-mini:before {
    font-family: "Material Design Icons";
    content: "\F1D9";
} */
</style>

    <!-- <header id="main-header" class="py-2 text-white">
    <div class="container">
      	<div class="row">
        	<div class="col-md-6">
				</div>
					<div class="col-md-6 mt-2">
						<?php if($this->session->userdata('user')->status == 0){?>
						<a href="<?php echo base_url('Business_partner/profileactivate')?>" class="btn btn-warning">
							<i class="fas fa-toggle-on"></i> Activate Account
						</a>
						<?php } ?>
					</div>
				</div>
    		</div>
  </header> -->

  <!-- ACTIONS -->
  <!-- <section id="actions" class="py-4 mb-4">
    <div class="container">
      
    </div>
  </section> -->

  <!-- POSTS -->
<section id="posts">
	

	<div class="row">
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini mb-4 bg-warning">
                        <div class="card-body">
                        
                          <h2 class="mb-1"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;<?php echo $this->session->userdata('user')->shopname?></h2>
                          <p>Shopname</p>
                          <div class="chartjs-wrapper">
                            <canvas id="barChart"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini  mb-4 bg-danger">
                        <div class="card-body">
                          <h2 class="mb-1"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;<?php echo $product;?></h2>
                          <p>Total Products</p>
                          <div class="chartjs-wrapper">
                            <canvas id="dual-line"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini mb-4 bg-info">
                        <div class="card-body">
                          <h2 class="mb-1"><?php echo $order;?></h2>
                          <p>Monthly Total Order</p>
                          <div class="chartjs-wrapper">
                            <canvas id="area-chart"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-sm-6">
                      <div class="card card-mini mb-4 bg-success">
                        <div class="card-body">
                          <h2 class="mb-1"> &#x20B9;&nbsp;<?php echo $sale;?></h2>
                          <p>Total Revenue This Year</p>
                          <div class="chartjs-wrapper">
                            <canvas id="line"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


						<!-- <div class="row">
							<div class="col-xl-8 col-md-12">
                      
                      <div class="card card-default">
                        <div class="card-header">
                          <h2>Sales Of The Year</h2>
                        </div>
                        <div class="card-body">
                          <canvas id="linechart" class="chartjs"></canvas>
                        </div>
                        <div class="card-footer d-flex flex-wrap bg-white p-0">
                          <div class="col-6 px-0">
                            <div class="text-center p-4">
                              <h4>$0.00</h4>
                              <p class="mt-2">Total orders of this year</p>
                            </div>
                          </div>
                          <div class="col-6 px-0">
                            <div class="text-center p-4 border-left">
                              <h4>$0.00</h4>
                              <p class="mt-2">Total revenue of this year</p>
                            </div>
                          </div>
                        </div>
                      </div>
</div>
							<div class="col-xl-4 col-md-12"> -->
                  
                  <!-- <div class="card card-default">
                    <div class="card-header justify-content-center">
                      <h2>Orders Overview</h2>
                    </div>
                    <div class="card-body" >
                      <canvas id="doChart" ></canvas>
                    </div>
                    <a href="#" class="pb-5 d-block text-center text-muted"><i class="mdi mdi-download mr-2"></i> Download overall report</a>
                    <div class="card-footer d-flex flex-wrap bg-white p-0">
                      <div class="col-6">
                        <div class="py-4 px-4">
                          <ul class="d-flex flex-column justify-content-between">
                            <li class="mb-2"><i class="mdi mdi-checkbox-blank-circle-outline mr-2" style="color: #4c84ff"></i>Order Completed</li>
                            <li><i class="mdi mdi-checkbox-blank-circle-outline mr-2" style="color: #80e1c1 "></i>Order Unpaid</li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-6 border-left">
                        <div class="py-4 px-4 ">
                          <ul class="d-flex flex-column justify-content-between">
                            <li class="mb-2"><i class="mdi mdi-checkbox-blank-circle-outline mr-2" style="color: #8061ef"></i>Order Pending</li>
                            <li><i class="mdi mdi-checkbox-blank-circle-outline mr-2" style="color: #ffa128"></i>Order Canceled</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
</div>
						</div> -->



						<div class="row">
							<div class="col-12"> 
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none recent-orders" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2>Recent Orders</h2>
                      <div class="date-range-report ">
                        <span></span>
                          
                      </div>
                    </div>
                    <div class="card-body pt-0 pb-5 mytable">
                      
                    </div>
                  </div>
</div>
						</div>

</section>

<script src="<?php echo base_url('assets/js/Chart.min.js')?>"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
$(function() {

/*======== 1. RECNT ORDERS ========*/
if ($("#recent-orders").length != 0) {
  var start = moment().subtract(29, "days");
  var end = moment();
  var cb = function(start, end) {
    $("#recent-orders .date-range-report span").html(
      start.format("ll") + " - " + end.format("ll")
    );
  };

  $("#recent-orders .date-range-report").daterangepicker(
    {
      startDate: start,
      endDate: end,
      opens: 'left',
      ranges: {
        Today: [moment(), moment()],
        Yesterday: [
          moment().subtract(1, "days"),
          moment().subtract(1, "days")
        ],
        "Last 7 Days": [moment().subtract(6, "days"), moment()],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "This Month": [moment().startOf("month"), moment().endOf("month")],
        "Last Month": [
          moment()
            .subtract(1, "month")
            .startOf("month"),
          moment()
            .subtract(1, "month")
            .endOf("month")
        ]
      }
    },
    cb
  );
  cb(start, end);
}

});


// $('.date-range-report').on('hide.daterangepicker', function(ev, picker) {
//   //do something, like clearing an input
//   console.log($('.date-range-report').val(''));
// });

$('.date-range-report').on('apply.daterangepicker', function(ev, picker) {
	var start = picker.startDate.format('YYYY-MM-DD');
	var end = picker.endDate.format('YYYY-MM-DD');
  loadtable(start,end)
});
var start = moment().subtract(29, "days").format('YYYY-MM-DD'); 
var end = moment().format('YYYY-MM-DD');



function loadtable(start,end){
	$.ajax({ 
		url: "<?= base_url('Business_partner/getteble')?>",
		data: {'start':start,'end':end},
		type: 'post',
		success: function(data){
			$('.mytable').html(data)
			// //alert(data);
			// console.log(data)
			// //location.reload();
		}
	});
}
$(document).ready(function(){
	loadtable(start,end)
});


 /*======== 3. LINE CHART ========*/
 var ctx = document.getElementById("linechart");
	  if (ctx !== null) {
	    var chart = new Chart(ctx, {
	      // The type of chart we want to create
	      type: "line",

	      // The data for our dataset
	      data: {
	        labels: [
	          "Jan",
	          "Feb",
	          "Mar",
	          "Apr",
	          "May",
	          "Jun",
	          "Jul",
	          "Aug",
	          "Sep",
	          "Oct",
	          "Nov",
	          "Dec"
	        ],
	        datasets: [
	          {
	            label: "",
	            backgroundColor: "transparent",
	            borderColor: "rgb(82, 136, 255)",
	            data: [
	              100,
	              11000,
	              10000,
	              14000,
	              11000,
	              17000,
	              14500,
	              18000,
	              5000,
	              23000,
	              14000,
	              19000
	            ],
	            lineTension: 0.3,
	            pointRadius: 5,
	            pointBackgroundColor: "rgba(255,255,255,1)",
	            pointHoverBackgroundColor: "rgba(255,255,255,1)",
	            pointBorderWidth: 2,
	            pointHoverRadius: 8,
	            pointHoverBorderWidth: 1
	          }
	        ]
	      },

	      // Configuration options go here
	      options: {
			  
	        responsive: false,
	        maintainAspectRatio: false,
	        legend: {
	          display: false
	        },
	        layout: {
	          padding: {
	            right: 10
	          }
	        },
	        scales: {
	          xAxes: [
	            {
	              gridLines: {
	                display: false
	              }
	            }
	          ],
	          yAxes: [
	            {
	              gridLines: {
	                display: true,
	                color: "#eee",
	                zeroLineColor: "#eee",
	              },
	            //   ticks: {
	            //     callback: function (value) {
	            //       var ranges = [
	            //         { divider: 1e6, suffix: "M" },
	            //         { divider: 1e4, suffix: "k" }
	            //       ];
	            //       function formatNumber(n) {
	            //         for (var i = 0; i < ranges.length; i++) {
	            //           if (n >= ranges[i].divider) {
	            //             return (
	            //               (n / ranges[i].divider).toString() + ranges[i].suffix
	            //             );
	            //           }
	            //         }
	            //         return n;
	            //       }
	            //       return formatNumber(value);
	            //     }
	            //   }
	            }
	          ]
	        },
	        tooltips: {
	          callbacks: {
	            title: function (tooltipItem, data) {
	              return data["labels"][tooltipItem[0]["index"]];
	            },
	            label: function (tooltipItem, data) {
	              return "$" + data["datasets"][0]["data"][tooltipItem["index"]];
	            }
	          },
	          responsive: true,
	          intersect: false,
	          enabled: true,
	          titleFontColor: "#888",
	          bodyFontColor: "#555",
	          titleFontSize: 12,
	          bodyFontSize: 18,
	          backgroundColor: "rgba(256,256,256,0.95)",
	          xPadding: 20,
	          yPadding: 10,
	          displayColors: false,
	          borderColor: "rgba(220, 220, 220, 0.9)",
	          borderWidth: 2,
	          caretSize: 10,
	          caretPadding: 15
	        }
	      }
	    });
	  }


	  var doughnut = document.getElementById("doChart");
	  if (doughnut !== null) {
	    var myDoughnutChart = new Chart(doughnut, {
	      type: "doughnut",
	      data: {
	        labels: ["completed", "unpaid", "pending", "canceled"],
	        datasets: [
	          {
	            label: ["completed", "unpaid", "pending", "canceled"],
	            data: [4100, 2500, 1800, 2300],
	            backgroundColor: ["#4c84ff", "#29cc97", "#8061ef", "#fec402"],
	            borderWidth: 1
	            // borderColor: ['#4c84ff','#29cc97','#8061ef','#fec402']
	            // hoverBorderColor: ['#4c84ff', '#29cc97', '#8061ef', '#fec402']
	          }
	        ]
	      },
	      options: {
	        responsive: false,
	        maintainAspectRatio: false,
	        legend: {
	          display: false
	        },
	        cutoutPercentage: 75,
	        tooltips: {
	          callbacks: {
	            title: function (tooltipItem, data) {
	              return "Order : " + data["labels"][tooltipItem[0]["index"]];
	            },
	            label: function (tooltipItem, data) {
	              return data["datasets"][0]["data"][tooltipItem["index"]];
	            }
	          },
	          titleFontColor: "#888",
	          bodyFontColor: "#555",
	          titleFontSize: 12,
	          bodyFontSize: 14,
	          backgroundColor: "rgba(256,256,256,0.95)",
	          displayColors: true,
	          borderColor: "rgba(220, 220, 220, 0.9)",
	          borderWidth: 2
	        }
	      }
	    });
	  }
</script>