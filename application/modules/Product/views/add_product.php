
 <?php if($this->session->flashdata('message')){?>
    <div class="alert alert-success">
        <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
    </div>
    <?php } ?>
    <div class="row">
    <div class ="col-md-8">
        <h2>Add Product</h2>
        <div style = "text-align: right";>
            <a href="<?= base_url().'Product/list_product';?>" class="btn btn-primary">List Of Product</a>
        </div>
        <form action="<?= base_url('Product/add_product_action')?>" method = "POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="email">Name:</label>
                <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo set_value('name'); ?>">
                <?php echo form_error('name'); ?>
            </div>
            <div class="form-group">
                <label for="pries">Price:</label>
                <input type="text" class="form-control" id="pries" placeholder="Enter pries" name="pries" value="<?php echo set_value('pries'); ?>">
                <?php echo form_error('pries'); ?>
            </div>
            <div class="form-group">
                <label for="pries">Quantity:</label>
                <input type="text" class="form-control" id="pries" placeholder="Enter pries" name="quantity" value="<?php echo set_value('quantity'); ?>">
                <?php echo form_error('quantity'); ?>
            </div>
            
            <div class="form-group">
                <label for="pwd">Description:</label>
                <input type="text" class="form-control" id="description" placeholder="Enter description" name="description" value="<?php echo set_value('description'); ?>">
                <?php echo form_error('description'); ?>
            </div>
            <div class="form-group"> 
                <label for="varchar">GST 
                    <?php echo form_error('gst') ?> 
                </label> 
                <select name="gst" class="form-control" style = "height: 34px"> 
                    <option value="">Select</option>
                    <?php
                    foreach($gsts as $value)
                    {
                        if($value->gst == $product->gst)
                        {
                            echo "<option selected='selected' value='".$value->gst."'>".$value->gst."</option>";
                        }
                        else
                        {
                            echo "<option value='".$value->gst."'>".$value->gst."</option>";
                        }
                    }
                    ?>
                </select> 
            </div> 
            <div class="form-group"> 
                <label for="varchar">Service 
                    <?php echo form_error('service') ?> 
                </label> 
                <select name="service" class="form-control service" style = "height: 34px"> 
                    <option value="">Select</option>
                    <?php
                    foreach($services as $value)
                    {
                        if($value->id == $service)
                        {
                            echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
                        }
                        else
                        {
                            echo "<option value='".$value->id."'>".$value->name."</option>";
                        }
                    }
                    ?>
                </select> 
            </div> 
            <div class="form-group"> 
                <label for="varchar">Sub Service 
                    <?php echo form_error('sub_service') ?> 
                </label> 
                <select name="sub_service" class="form-control service1" style = "height: 34px"> 
                    <option value="">Select</option>
                    <?php
                    foreach($subservices as $value)
                    {
                        if($value->id == $subservice)
                        {
                            echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
                        }
                        else
                        {
                            echo "<option value='".$value->id."'>".$value->name."</option>";
                        }
                    }
                    ?>
                </select> 
            </div> 
        <div>
        </div>
            <div class="form-group">
                <label for="pwd">Image:</label> 
                <input type="file" class="form-control" id="fname" name="fname[]"> 
            <div class="input_fields_wrap">

            </div>
            <button class="add_field_button">Add More</button>
            <?php echo form_error('fname'); ?>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
<div>
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper       = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><input type="file" class="form-control" id="fname" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });


$(document).ready(function(){
  $(".service").change(function(){
    var service = $(".service").val();
    
    $.ajax({
            url: "<?php echo base_url('Product/getsubservice');?>",
            method: "POST",
            data: {service: service},
            cache: true
        }).success(function(data){
           
            data = $.parseJSON(data);
            var i;
            var html='';
            for (i = 0; i < data.length; i++) {
            html += "<option value = '"+data[i].id+"'>"+data[i].name+"</option>"
           }
          
           $(".service1").html(html);
         
        }).fail(function(data){
            console.log(data);
        });
  });
});
</script>