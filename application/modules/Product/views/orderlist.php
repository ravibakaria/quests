
<style>
.card-mini .card-body h2 {
    color: rgb(27, 34, 60);
    font-size: 1.49625rem;
}

.mb-1, .my-1 {
    margin-bottom: 0.25rem !important;
}

.card-default .card-header {
    padding-left: 1.88rem;
    padding-right: 1.88rem;
    padding-top: 3rem;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    background-color: #ffffff;
    border-bottom: none;
}
.card-header:first-child {
    border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
.card-table-border-none .card-header {
    padding-left: 1.88rem;
    padding-right: 1.88rem;
    padding-top: 3rem;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    background-color: #ffffff;
    border-bottom: none;
}
.justify-content-between {
    justify-content: space-between !important;
}
.card-table-border-none .card-body {
    padding: 1.9rem 1.88rem;
}
.table-responsive {
    display: table;
    overflow-x: auto;
}
.card-table {
    margin-top: 3.12rem;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #8a909d;
}
.date-range-report span:after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 1em;
    content: "";
    border-right: 5px solid transparent;
    border-bottom: 0px;
    border-top: 6px solid;
    border-left: 5px solid transparent;
    vertical-align: middle;
    
}
.dropdown-toggle::after {
    display: none;
}
@media screen and (min-width: 1200px) {
    #linechart {
    min-height: 280px !important;
}
  }
  


/* .widget-dropdown .dropdown-toggle.icon-burger-mini:before {
    font-family: "Material Design Icons";
    content: "\F1D9";
} */
</style>

   
<section id="posts">
	

	


						<div class="row">
							<div class="col-12"> 
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none recent-orders" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2>Recent Orders</h2>
                      <div class="date-range-report ">
                        <span></span>
                          
                      </div>
                    </div>
                    <div class="card-body pt-0 pb-5 mytable">
                      
                    </div>
                  </div>


</section>

<script src="<?php echo base_url('assets/js/Chart.min.js')?>"></script>
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
$(function() {

/*======== 1. RECNT ORDERS ========*/
if ($("#recent-orders").length != 0) {
  var start = moment().subtract(29, "days");
  var end = moment();
  var cb = function(start, end) {
    $("#recent-orders .date-range-report span").html(
      start.format("ll") + " - " + end.format("ll")
    );
  };

  $("#recent-orders .date-range-report").daterangepicker(
    {
      startDate: start,
      endDate: end,
      opens: 'left',
      ranges: {
        Today: [moment(), moment()],
        Yesterday: [
          moment().subtract(1, "days"),
          moment().subtract(1, "days")
        ],
        "Last 7 Days": [moment().subtract(6, "days"), moment()],
        "Last 30 Days": [moment().subtract(29, "days"), moment()],
        "This Month": [moment().startOf("month"), moment().endOf("month")],
        "Last Month": [
          moment()
            .subtract(1, "month")
            .startOf("month"),
          moment()
            .subtract(1, "month")
            .endOf("month")
        ]
      }
    },
    cb
  );
  cb(start, end);
}

});


// $('.date-range-report').on('hide.daterangepicker', function(ev, picker) {
//   //do something, like clearing an input
//   console.log($('.date-range-report').val(''));
// });

$('.date-range-report').on('apply.daterangepicker', function(ev, picker) {
	var start = picker.startDate.format('YYYY-MM-DD');
	var end = picker.endDate.format('YYYY-MM-DD');
  loadtable(start,end)
});
var start = moment().subtract(29, "days").format('YYYY-MM-DD'); 
var end = moment().format('YYYY-MM-DD');



function loadtable(start,end){
	$.ajax({ 
		url: "<?= base_url('Admin/getteble')?>",
		data: {'start':start,'end':end},
		type: 'post',
		success: function(data){
			$('.mytable').html(data)
			// //alert(data);
			// console.log(data)
			// //location.reload();
		}
	});
}
$(document).ready(function(){
	loadtable(start,end)
});



</script>