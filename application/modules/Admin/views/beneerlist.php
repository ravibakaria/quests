<!doctype html> 
<html> 
<head> 
    <title>DataTables and Codeigniter</title> 
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> -->


<!--Data Table-->


<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Banners List</h2> 
        </div> 
        <div class="col-md-4 text-center"> 
            <div style="margin-top: 4px" id="message"> 
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?> 
            </div> 
        </div> 
        <div class="col-md-4 text-right"> 
            <?php echo anchor(base_url('Admin/Admin/create_banner'), 'Create', 'class="btn btn-primary"'); ?> 
        </div> 
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Banner</th> 
                <th>Added On</th> 
                <th>Status</th> 
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($banner as $service) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <img src="<?php echo base_url('assets/images/').$service->name ?>" alt="<?php echo $service->name ?> " class= "img img-thumbnail" width="70" height="50">
                    </td>  
                    <td> 
                        <?php echo $service->created_at ?> 
                    </td> 
                    <td> 
                        <?php 
                        if($service->status == 1){
                            echo anchor(base_url('Admin/Admin/inactive_banner/'.$service->id),'Active');
                        } else {
                            echo anchor(base_url('Admin/Admin/active_banner/'.$service->id),'Inactive');
                        }
                         
            ?> 
                    </td> 
                    <td style="text-align:center" width="200px"> 
                        <?php  
             
            echo anchor(base_url('Admin/Admin/delete_banner/'.$service->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');  
            ?> 
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>