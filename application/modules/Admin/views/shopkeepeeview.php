
<div class="row">
    <div class ="col-md-8 card mx-auto ">
        <div class=" card-body">
        <h3 class = "text-center">Shopkeeper Details </h3>
        <div class="form-group">
                <label for="email">Name:</label>
                <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->name) .' ' .$shopkeepers->lname ?>" readonly>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <label>Email</label>
                    <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->email) ?>" readonly>
                   
                </div> 
                <div class="col form-group">
                    <label>Mobile No.</label>
                    <input type="text" class="form-control"  value ="<?php echo $shopkeepers->phone ?>" readonly>
                </div> 
            </div> 
            <div class="form-row">
                <div class="col form-group">
                    <label>Gender:</label>
                    <input type="text" class="form-control"  value="<?php echo $shopkeepers->gender; ?>" readonly >
                </div> 
                <div class="col form-group">
                    <label>City:</label>
                    <input type="text" class="form-control" value="<?php echo $shopkeepers->city ?>" readonly >
               
                </div> 
            </div> 
           

            <div class="form-group">
                <label for="pwd">Shop Address :</label>
                <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->shopadd); ?>" readonly >
                
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <label>Shop Name:</label>
                    <input type="text" class="form-control"   value="<?php echo ucfirst($shopkeepers->shopname); ?>" readonly >
                </div> 
                <div class="col form-group">
                    <label>Shop No :</label>
                    <input type="text" class="form-control"   value="<?php echo ucfirst($shopkeepers->shopno); ?>" readonly >
                <?php echo form_error('quantity'); ?>
                </div> 
            </div> 
            <div class="form-row">
                <div class="col form-group">
                    <label>Bussines Module:</label>
                    <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->bussines_module); ?>" readonly >
                </div> 
                <div class="col form-group">
                    <label>Bussines Type :</label>
                    <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->bussines_type); ?>" readonly >
               
                </div> 
            </div> 
            <div class="form-row">
                <div class="col form-group">
                    <label>Bussines Type 1:</label>
                    <input type="text" class="form-control" value="<?php echo ucfirst($shopkeepers->bussines_type1); ?>" readonly >
                </div> 
                <div class="col form-group">
                    <label>Bussines Type 2 :</label>
                    <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->bussines_type2); ?>" readonly >
               
                </div> 
            </div> 
            <div class="form-row">
                <div class="col form-group">
                    <label> Pin :</label>
                    <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->pin); ?>" readonly >
                </div> 
                <div class="col form-group">
                    <label>Aria In K.M :</label>
                    <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->aria); ?>" readonly >
                </div> 
            </div> 
            <div class="form-row">
                <div class="col form-group">
                    <label>Latitude:</label>
                    <input type="text" class="form-control" value="<?php echo ucfirst($shopkeepers->latitude); ?>" readonly >
                </div> 
                <div class="col form-group">
                    <label>Longitude:</label>
                    <input type="text" class="form-control"  value="<?php echo ucfirst($shopkeepers->longitude); ?>" readonly >
               
                </div> 
            </div> 
            <header class="mb-4"><h4 class="card-title">Bank Details</h4></header>

				<div class="form-group">
					<label>Bank Account Holder Name</label>
					<input type="text" class="form-control" placeholder="" name = "holdername"  value = "<?php echo $shopkeepers->holdername;?>" readonly>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Bank Name</label>
					<input type="text" class="form-control" placeholder="" name = "bankname" value = "<?php echo $shopkeepers->bankname;?>" readonly>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Account No.</label>
					<input type="text" class="form-control" placeholder="" name = "accountno" value = "<?php echo $shopkeepers->accountno;?>" readonly>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>IFSC Code</label>
					<input type="text" class="form-control" placeholder="" name = "ifsc"  value = "<?php echo $shopkeepers->ifsc;?>" readonly>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Google Pay Number Or UPI</label>
					<input type="text" class="form-control" placeholder="" name = "upi" value = "<?php echo $shopkeepers->upi;?>" readonly>
				</div> <!-- form-group end.// -->
            <label>KYC Images:</label>
            <div class="row">
            <?php if($shopkeepers->img1 !=''){?>
                <div class="col-md-3">
                    <img src="<?= base_url().'assets/images/users/'.$shopkeepers->img1?>" class = "img img-thumbnail">
                </div>
            <?php } ?>
            <?php if($shopkeepers->img2 !=''){?>
                <div class="col-md-3">
                    <img src="<?= base_url().'assets/images/users/'.$shopkeepers->img3?>" class = "img img-thumbnail">
                </div>
            <?php } ?>
            <?php if($shopkeepers->img3 !=''){?>
                <div class="col-md-3">
                    <img src="<?= base_url().'assets/images/users/'.$shopkeepers->img3?>" class = "img img-thumbnail">
                </div>
            <?php } ?>
            <?php if($shopkeepers->img3 !=''){?>
                <div class="col-md-3">
                    <img src="<?= base_url().'assets/images/users/'.$shopkeepers->img4?>" class = "img img-thumbnail">
                </div>
            <?php } ?>
			</div>
        </div>
    </div>
</div>

