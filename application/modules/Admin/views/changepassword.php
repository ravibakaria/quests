<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<style>
    .error{
        color:red;
    }

</style>
<form class="userform" id="userForm" method="post" action="<?php echo base_url('Admin/changepass_action')?>">

<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-th"></span>
                        Change password   
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                       
                        <div style="margin-top:40px;" class="col-xs-6 col-sm-6 col-md-6 login-box">
                         
                          <div class="form-group">
                          <input id="password" name="password" type="password" class="form-control" placeholder="New Password">
                             
                          </div>
                          <div class="form-group">
                              <input id="confirm_password"  class="form-control"name="confirm_password" type="password" placeholder="Confirm Password">
                            
                          </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <button class="btn icon-btn-save btn-success" type="submit">
                            <span class="btn-save-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
    $j("#userForm").validate({
        rules: {
           
            password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            }
        },
        messages: {
          
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            }
        }
    });
});
</script>