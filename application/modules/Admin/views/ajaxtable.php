<table class="table card-table table-responsive table-responsive-large" style="width:100%">
                        <thead>
                          <tr>
                            <th>Order ID</th>
                            <th>Product Name</th>
                            <th class="d-none d-lg-table-cell">Units</th>
                            <th class="d-none d-lg-table-cell">Order Date</th>
                            <th class="d-none d-lg-table-cell">Order Cost</th>
                            <th>Status</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $i = 0;
                            foreach ($order as $o){ $i++;?>
                          <tr>
                            <td ><?php echo $o->invoice_no;?></td>
                            <td >
                              <a class="text-dark" href=""> <?php echo $o->product_name;?></a>
                            </td>
                            <td class="d-none d-lg-table-cell"><?php echo $o->qty;?></td>
                            <td class="d-none d-lg-table-cell"><?php  echo date('d-F-Y',strtotime($o->date));?></td>
                            <td class="d-none d-lg-table-cell"> &#x20B9;<?php echo $o->total_amt;?></td>
                            <td >
                            <?php  if($o->delivarystua == 0){
                                $status = 'Pending';
                                $class = 'badge-warning';
                            }
                            if($o->delivarystua == 1){
                                $status = 'Delivered';
                                $class = 'badge-success';
                            }
                            if($o->delivarystua == 2){
                                $status = 'Shipped';
                                $class = 'text-info';
                            }
                            if($o->delivarystua == 3){
                                $status = 'Cancelled';
                                $class = 'badge-danger';
                            }
                    ?>
                            <span class="badge <?php echo $class;?>"><?php echo $status?></span> 
                             
                            </td>
                            <td class="text-right">
                              <div class="dropdown show d-inline-block widget-dropdown">
                                <a class="dropdown-toggle icon-burger-mini" href="" role="button" id="dropdown-recent-order1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-recent-order1">
                                  <li class="dropdown-item">
                                    <a href="#">View</a>
                                  </li>
                                  <li class="dropdown-item">
                                    <a href="#">Remove</a>
                                  </li>
                                </ul>
                              </div>
                            </td>
                          </tr>
                        <?php } ?>
                          
                        </tbody>
                      </table>