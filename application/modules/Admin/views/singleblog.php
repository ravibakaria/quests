    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 

<div class="row">
    <div class="col-md-8 card mx-auto ">
        <div class="card-header">
            <h2><?php echo $blog->heading?></h2>
        </div>
        <div class="card-body">
        <?php echo $blog->body?>
        <p><?php echo $blog->date?></p>
        </div>
    </div>
</div>
