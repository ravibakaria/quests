<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <h2 style="margin-top:0px">About Us Page template </h2> 
    <form action="<?php echo $action; ?>" method="post"> 
        <div class="form-group"> 
            <label for="varchar">Body 
                <?php echo form_error('bodu') ?> 
            </label> 
            <textarea class="form-control" id="description" placeholder="Enter description" name="body" value="<?php echo set_value('description'); ?>" cols="30" rows="5" required ><?php echo $body?></textarea>
        
        </div>  
        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
        <button type="submit" class="btn btn-primary"> 
            Save
        </button> 
        
    </form> 

<script>

CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>
