<!doctype html> 
<html> 
<head> 
  
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> -->


<!--Data Table-->


<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Shopkeepers List</h2> 
        </div> 
         
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Name</th> 
                <th>A/C No.</th> 
                <th>Phone</th> 
                <th>Email</th> 
                <th>City</th> 
                <th>Password</th> 
                <th>Status</th> 
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($shopkeepers as $service) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <?php echo $service->name.' '.$service->lname ?> 
                    </td>  
                    <td> 
                        SK0<?php echo $service->id ?> 
                    </td>  
                    <td> 
                        <?php echo $service->phone; ?> 
                    </td>  
                    <td> 
                        <?php  echo $service->email ?> 
                    </td>  
                    
                    <td>
                        <?php  echo $service->city ?> 
                        
                    </td> 
                    <td>
                        <?php  echo $service->pass1 ?> 
                        
                    </td> 
                    <td>
                        <?php 
                        if( $service->status == 1){
                            echo "Active";
                        }else{
                            echo "InActive";
                        }
                        
                         ?> 
                        
                    </td> 
                    <td>
                    <a href='<?php echo base_url()."Admin/Show_shopkeeper/".$service->id;?>' class='btn btn-primary'>View</a> 
                    <?php if($service->isdelete == 0){?>
                    <a href='<?php echo base_url()."Admin/delete_shopkeeper/".$service->id;?>' class='btn btn-primary' onclick="return confirm('Are You Sure ?')">Delete</a>
                    <?php } else{ ?>
                        <a href='<?php echo base_url()."Admin/active_shopkeeper/".$service->id;?>' class='btn btn-primary' onclick="return confirm('Are You Sure ?')">Active Now</a>   
                    <?php } ?>                     
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>