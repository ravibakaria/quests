<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Blogs List</h2> 
        </div> 
        <div class="col-md-4 text-center"> 
            <div style="margin-top: 4px" id="message"> 
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?> 
            </div> 
        </div> 
        <div class="col-md-4 text-right"> 
            <?php echo anchor(base_url('Admin/Blog/create_blog'), 'Create', 'class="btn btn-primary"'); ?> 
        </div> 
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Heading</th> 
                <th>Blog content</th> 
                <th>Created Date</th> 
                <th>Status</th> 
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            function readMoreHelper($story_desc,$id, $chars = 500) {
                $story_desc = substr($story_desc,0,$chars);  
                $story_desc = substr($story_desc,0,strrpos($story_desc,' '));  
                $story_desc = $story_desc.".... <a href='".base_url('Admin/Blog/blog/').$id."'>Read More</a>";  
                return $story_desc;  
            }
            $start = 0; 
            foreach ($blogs as $blog) 
            { 
                ?>  
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <?php echo $blog->heading ?> 
                    </td> 
                    <td> 
                        <?php echo readMoreHelper($blog->body,$blog->id) ?> 
                    </td>  
                    <td> 
                        <?php echo $blog->date ?> 
                    </td> 
                    <td> 
                        <?php 
                        if($blog->status == 1){
                            echo anchor(base_url('Admin/Blog/inactive_blog/'.$blog->id),'Active');
                        } else {
                            echo anchor(base_url('Admin/Blog/active_blog/'.$blog->id),'Inactive');
                        }
                         
            ?> 
                    </td> 
                    <td style="text-align:center" width="200px"> 
                        <?php  
              echo anchor(base_url('Admin/Blog/edit_blog/'.$blog->id),'Edit |');  
            echo anchor(base_url('Admin/Blog/delete_blog/'.$blog->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');  
            ?> 
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>