<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
<style type="text/css">
#mytable_length,#mytable_info,#mytable_paginate {
    display: none;
}

#mytable_filter {
    display: none;
}

</style>


<!-- Search form (start) -->

<br/>
<div class="row">
    <div class = "col">
        <h3>List Of Advertisement</h3>
    </div>
    <div  class = "col" style = "text-align: right";>
        <form method='post' action="<?= base_url() ?>Admin/loadRecord2" >
            <input type='text' name='search' value='<?= $search ?>'>
            <input type='submit' name='submit' value='Search'>
        </form>
    </div>
</div>
<!-- Posts List -->

<table class="table table-bordered" id ="mytable">
  <thead>
    <tr>
      <th scope="col">S.no</th>
      <th scope="col">Name</th>
      <th scope="col">Business Type</th>
      <th scope="col">Image</th>
      <th scope="col">Content</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
    <?php 
        $sno = $row+1;
        foreach($result as $data){
            $serviceId = $this->db->get_where('bussines_partner',['id'=>$data['u_id']])->row();
            $business = $this->db->get_where('service',['id'=>$serviceId->bussines_type])->row();
            $content = substr($data['discription'],0, 180)." ...";
            echo "<tr>";
            echo "<td>".$sno."</td>";
            echo "<td>".$data['name']."</td>";
            echo "<td>".$business->name."</td>";
           
            ?>
            <td>
            <img class = "img img-thumbnail" src="<?= base_url().'assets/images/'.$data['img_name']?>" height = "170px" width = "170px">
            </td>
           <?php echo "<td>".$content."</td>"; ?>
           <td>
           <?php if($data['status'] ==1){
               echo "Active";
           }else{
               echo "Inactive";
           }
               ?>
            </td>
           <td>
           <a href="<?php echo base_url('Admin/edit_advertisement/').$data['id']; ?>" class="btn btn-primary"> Edit </a>
					<a href="<?php echo base_url('Admin/delete_advertisement/').$data['id']; ?>" class="btn btn-outline-light" onclick="return confirm('Are You Sure ')">Delete</a>
            </td>
            </tr>

            <?php
            $sno++;

        }
        if(count($result) == 0){
            echo "<tr>";
            echo "<td colspan='3'>No record found.</td>";
            echo "</tr>";
        }
    ?>
</table>

<!-- Paginate -->
<div style = "text-align: right; margin-top: 10px;">
    <?= $pagination; ?>
</div>
<div style='margin-top: 10px;'>

</div>
<script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
</script> 