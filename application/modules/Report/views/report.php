


  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  
</head>
<body>

<?php 


// $begin = new DateTime( '2019-05-21' );
// $end = new DateTime( '2019-06-04' );
// $end = $end->modify( '+1 day' ); 

// $interval = new DateInterval('P1D');
// $daterange = new DatePeriod($begin, $interval ,$end);
 $data = [];
// foreach($daterange as $date){
//      $day = $date->format("Y-m-d");
//   foreach ($payments as $key => $value) {
//     $date = $value->orderDate;
//     //if ( strtotime($day) == strtotime($date) ){
//     if($day == $date){
//       // echo $day.'<br>';
//       // echo $value->amt.'<br>';
//       $data [$day]= $value->amt;
//    }  
//   } 
//  }
  //  echo "<pre>";
  // print_r($data);
?>

<div class="container">
  <div class="row">
    <div class ="col-md-4">
      <div class="form-group">
        <input type="text" class="form-control" id="datepicker" placeholder="Start Date" name="to" autocomplete="off">
      </div>
    </div>
    <div class ="col-md-4">
      <div class="form-group">
        <input type="text" class="form-control" id="datepicker1" placeholder="End Date" name="from" autocomplete="off">
      </div>
    </div>
    <!-- <div class ="col-md-2">
      <div class="form-group">
      <input type="checkbox" id = "bar">
        <button type="button" class="btn btn-success">Bar chart view</button> 

      </div>
    </div> -->
    <div class ="col-md-2">
      <div class="form-group">
        <button type="button" class="btn btn-success getsale">GET SALE</button>
      </div>
    </div>
  </div>
  <div id="test">
     <div id="linechart" style="min-width: 310px;  margin: 0 auto"></div>
  </div>
  

<script type="text/javascript">
var $j = jQuery.noConflict();
  $j(function() {
    $j( "#datepicker" ).datepicker({
      dateFormat: "dd-mm-yy",
      maxDate: 0 
    });
  } );
  $j(function() {
    $j( "#datepicker1" ).datepicker({
      dateFormat: "dd-mm-yy",
      maxDate: 0 
    });

  } );

// $('.getsale').click(function() {
//   var to = $('#datepicker').val();
//   var from = $('#datepicker1').val();
//   //alert(from); return false;
//   if(to == ''){
//     alert('Enter start date');return false;
//   }
//    if(from == ''){
//     alert('Enter end date');return false;
//   }
//     $.ajax({ 
//         url: "<?= base_url('Report/getsale')?>",
//         data: {'to':to,'from':from },
//         type: 'post',
//         success: function(data){
//             alert(data);

//           //$("#test").html(chart);
//           //$("#test1").css('display','none');
//           //$("#test").css("display", "block");
//           //$("#tset").show();
            
//           }
//     });
// });

</script>
<script type="text/javascript">
var highchart =[];
   
    var options = {
        chart: {
            renderTo: 'linechart',
            type: 'line',
            zoomType: 'xy',
            height: 500,
            animation:{
                duration:600,
            }
        },
        navigation: {
            buttonOptions: {
                symbolStroke: '#6fb04d',
                theme: {
                    r: 0,
                    states: {
                        hover: {
                            fill: '#94bb80',
                            stroke: '#fff',
                        },
                        select: {
                            stroke: '#fff',
                            fill: '#94bb80'
                        }
                    }
                }
            }
        },
        exporting: {
            buttons: {
                contextButton: {
                    text: '<div>Print / Download</div>'
                }
            }
        },
        loading: {
            hideDuration: 1000,
            showDuration: 1000,
            labelStyle: {
                    color: 'black',
                    fontSize:'20px',
                    
                },
        },
        title: {
            text: 'Sales in linechart last Week',
        },
        xAxis: {
            type: 'datetime',
            startOnTick: true,
            categories: [<?php foreach ($payments as $key => $value) {
          $days = date('d', strtotime($value->orderDate));echo $days; echo ","; }?>],

            "gridLineWidth": 0,
            labels:{rotation: -0}
        },

        yAxis: {
            title: {
                text: 'Revenue($)'
            },
             "gridLineWidth": 0,
             lineWidth: 1,
           
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                    this.x +': $'+ this.y;
            }
        },
        plotOptions: {
            series: {
                marker: {
                    radius: 0
                }
            }
        },
         series: [{
            name: 'Sale',
            data: [<?php foreach ($payments as $key => $value) {echo $value->amt; echo ","; }?>]
          }],

    }
  
    chart = new Highcharts.Chart(options);
   
    







$j(".getsale").unbind('click').bind('click', function (e) {
        e.preventDefault(); 
        e.stopPropagation();
         var to = $('#datepicker').val();
         var from = $('#datepicker1').val();
        if($('#bar').prop("checked") == true){
                var chart = 'column';
            }
            else if($('#bar').prop("checked") == false){
                var chart = 'line';
            }
        $j.triggergraph(to,from,chart);
         //alert(to);
        return false;

        });
$j.triggergraph = function(to,from,chart){
  if(to == ''){
    alert('Enter start date');return false;
  }
   if(from == ''){
    alert('Enter end date');return false;
  }
  $.ajax({ 
        url: "<?= base_url('Report/getsale')?>",
        data: {'to':to,'from':from ,'bar':chart },
        type: 'post',
        success: function(data){

            obj = JSON.parse(data);
            //console.log(obj.amt);
              $j('#linechart').highcharts().hideLoading();
              var optionsChart = {
                  chart: {
                      renderTo: 'linechart',
                      type: obj.chart,
                      zoomType: 'xy',
                      height: 500,
                      animation:{
                          duration:600,
                      }
                  },
                  navigation: {
                      buttonOptions: {
                          symbolStroke: '#6fb04d',
                          theme: {
                              r: 0,
                              states: {
                                  hover: {
                                      fill: '#94bb80',
                                      stroke: '#fff',
                                  },
                                  select: {
                                      stroke: '#94bb80',
                                      fill: '#94bb80'
                                  }
                              }
                          }
                      }
                  },
                  exporting: {
                      buttons: {
                          contextButton: {
                              text: '<div>Print / Download</div>'
                          }
                      }
                  },
                  loading: {
                      hideDuration: 1000,
                      showDuration: 1000,
                      labelStyle: {
                              color: 'black',
                              fontSize:'20px',
                              
                          },
                  },
                  title: {
                      text: 'Sales in '+ obj.chart+' chart',
                  },
                  xAxis: {
                      type: 'datetime',
                      startOnTick: true,
                      categories: obj.categories,
                      "gridLineWidth": 0,
                      labels:{rotation: -0}
                  },
                  yAxis: {
                      title: {
                          text: 'Revenue($)',
                      },
                      "gridLineWidth": 0,
                      lineWidth: 1,
                  },
                  
                  credits: {
                      enabled: false
                  },
                  plotOptions: {
                      series: {
                          marker: {
                              radius: 0
                          }
                      }
                  },
                  tooltip: {
                      formatter: function() {
                              return '<b>'+ this.series.name +'</b><br/>'+
                              this.x +': $'+this.y;
                      }
                  },
                  series: [{
                      name: 'Sale',
                       data: obj.amt
                    }],
              };
              chart = new Highcharts.Chart(optionsChart);

            //$("#test").html(chart);
            //$("#test1").css('display','none');
            //$("#test").css("display", "block");
            //$("#tset").show();
              
            }
      });

}





</script>
