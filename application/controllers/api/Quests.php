<?php
   if (isset($_SERVER['HTTP_ORIGIN'])) {

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
    header('Content-Type: application/json');
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Razorpay.php';
use Razorpay\Api\Api;
class Quests extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
        parent::__construct();
       $this->load->model('APImodel');
      
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	// public function index_get($id = 0)
	// {
    //     if(!empty($id)){
    //         $data = $this->db->get_where("users", ['id' => $id])->row_array();
    //     }else{
    //         $data = $this->db->get("users")->result();
    //     }
    //     $data1 = [
    //         'results' =>$data,
    //         'status' => 1,
    //         'msg' =>'okkkkkk'
    //     ];
    //     $this->response($data1);
        
	// }
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    // public function index_post()
    // {
    //     $input = $this->input->post();
    //     $this->db->insert('users',$input);
     
    //     $this->response(['users created successfully.'], REST_Controller::HTTP_OK);
    // } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    // public function index_put($id)
    // {
    //     $input = $this->put();
    //     // echo "<pre>";
    //     // print_r($input);die;
    //     $this->db->update('users', $input, array('id'=>$id));
    //  echo $this->db->last_query();die;
    //     $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    // }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    // public function index_delete($id)
    // {
    //     $this->db->delete('users', array('id'=>$id));
       
    //     $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    // }

    public function serch_get(Type $var = null)
    {   $apikey = $_GET['apikey'];
        if($apikey == 'ravi'){
            $word = $_GET['s'];
            $re ='';
            if($word !=''){
                $this->db->select('id,name');
                $this->db->from('products');
                $this->db->like('name',$word);
                $this->db->group_by('name');
                $this->db->limit('8');
                $query = $this->db->get();
                // echo $this->db->last_query();exit();
                $re=  $query->result();
            }
           
            $data1 = [
                'data' =>$re,
                'status' => 1,
                'msg' =>'okkkkkk'
            ];
            $this->response($data1);
        // $this->response(['data' => $re], REST_Controller::HTTP_OK);
        }else{
            $this->response(['error' => 'No Data Found'], REST_Controller::HTTP_BAD_REQUEST);
        }
        
    }


    public function singleProduct_get($id,$token)
	{
       
        $c ='ADD TO CART';
        
        if($token !=''){
            $user = $this->db->get_where("users", ['token' => $token])->row();
            //echo $this->db->last_query();die;
            if(!empty($user)){
                $userid =  $user->id;
                //$cart = $this->db->get_where("cart", ['uid' => $userid])->count_all_results();
                $cart = $this->db->get_where("cart", ['pid' => $id,'uid'=>$userid])->row_array();
                //echo $this->db->last_query();die;
                if(!empty($cart)){
                    $c = "ADDED";
                }
            }
            
        }
        $data = [];
        $query = "SELECT products.id,products.name,products.description,products.pries,images.img_name FROM `products` LEFT JOIN images ON images.product_id = products.id WHERE products.id ='$id' GROUP BY products.id";
        $data =  $this->db->query($query)->result();
        $product  = $this->db->get_where('images', array('product_id' => $id))->result();
        $data2 = [];
        $data3 = [];
        foreach($data as $v){
               
                $string = strip_tags($v->description);
                if (strlen($string) > 250) {
                    $stringCut = substr($string, 0, 250);
                    $endPoint = strrpos($stringCut, ' ');
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '...';
                }
            $d = [
                'id' => $v->id,
                'name' =>$v->name,
                'pries' =>$v->pries,
                'c' => $c,
                'description' =>strip_tags($v->description),
                'img_name' =>base_url('assets/images/').$v->img_name,
            ];
            $data2[]=$d;
        }
        foreach($product as $v){
            $m = [
                'img_name' =>base_url('assets/images/').$v->img_name,
            ];
            $data3[]=$m;
        }
        $data1 = [
            'results' =>$data2,
            'imgs'=>$data3,
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }
    
    public function PopularProducts_get()
    {
       
        $query = "SELECT products.id,products.name,products.description,products.pries,products.pries,images.img_name FROM `products` LEFT JOIN images ON images.product_id = products.id GROUP BY products.id ORDER BY RAND() LIMIT 4";
        $data =  $this->db->query($query)->result();
        $data2 = [];
        foreach($data as $v){
                 $string = strip_tags($v->description);
                if (strlen($string) > 250) {
                    $stringCut = substr($string, 0, 250);
                    $endPoint = strrpos($stringCut, ' ');
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '...';
                }
            $d = [
                'id' => $v->id,
                'name' =>$v->name,
                'pries'=>$v->pries,
                'description' =>$string,
                'img_name' =>base_url('assets/images/').$v->img_name,
            ];
            $data2[]=$d;
        }
        $data1 = [
            'results' =>$data2,
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }

    public function Categories_get()
    {
       
        $query = "SELECT service.id,service.name, products.id as pid FROM `products` LEFT JOIN service ON products.service = service.id GROUP BY service.id ORDER BY RAND() LIMIT 4";
        $data =  $this->db->query($query)->result();
       
        $data1 = [
            'results' =>$data,
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }


    public function Recommended_get()
    {
       
        $query = "SELECT products.id,products.name,products.description,products.pries,images.img_name FROM `products` LEFT JOIN images ON images.product_id = products.id GROUP BY products.id ORDER BY RAND() LIMIT 5";
        $data =  $this->db->query($query)->result();
        $data2 = [];
        foreach($data as $v){
                 $string = strip_tags($v->description);
                if (strlen($string) > 250) {
                    $stringCut = substr($string, 0, 250);
                    $endPoint = strrpos($stringCut, ' ');
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '...';
                }
            $d = [
                'id' => $v->id,
                'name' =>$v->name,
                'pries'=>$v->pries,
                'description' =>$string,
                'img_name' =>base_url('assets/images/').$v->img_name,
            ];
            $data2[]=$d;
        }
        $data1 = [
            'results' =>$data2,
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }

    public function Categoriess_get()
    {
        $query = "SELECT service.id,service.name, products.id as pid FROM `products` LEFT JOIN service ON products.service = service.id GROUP BY service.id";
        $data =  $this->db->query($query)->result();
       
        $data1 = [
            'results' =>$data,
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }
    public function products_by_Category_get($id,$page,$limit)
    {
        // $page = $_GET['_page'];
        // $limit = $_GET['limit'];
        $query = "SELECT products.id,products.name,products.description,products.pries,images.img_name FROM `products` LEFT JOIN images ON images.product_id = products.id WHERE products.service = '$id' AND products.status = '1' GROUP BY products.id  LIMIT $page,$limit";
        //echo $query;die;
        $data =  $this->db->query($query)->result();
        $product  = $this->db->get_where('service', array('id' => $id))->row();
        $data2 = [];
        foreach($data as $v){
                 $string = strip_tags($v->description);
                if (strlen($string) > 50) {
                    $stringCut = substr($string, 0, 50);
                    $endPoint = strrpos($stringCut, ' ');
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '...';
                }
            $d = [
                'id' => $v->id,
                'name' =>$v->name,
                'description' =>$string,
                'pries'=>$v->pries,
                'img_name' =>base_url('assets/images/').$v->img_name,
            ];
            $data2[]=$d;
        }
        $data1 = [
            'results' =>$data2,
            'name' => $product->name,
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }


    public function products_show_more_get($page,$limit)
    {
        // $page = $_GET['_page'];
        // $limit = $_GET['limit'];
        $query = "SELECT products.id,products.name,products.description,products.pries,images.img_name FROM `products` LEFT JOIN images ON images.product_id = products.id WHERE  products.status = '1' GROUP BY products.id  LIMIT $page,$limit";
        //echo $query;die;
        $data =  $this->db->query($query)->result();
       
        $data2 = [];
        foreach($data as $v){
                 $string = strip_tags($v->description);
                if (strlen($string) > 50) {
                    $stringCut = substr($string, 0, 50);
                    $endPoint = strrpos($stringCut, ' ');
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '...';
                }
            $d = [
                'id' => $v->id,
                'name' =>$v->name,
                'description' =>$string,
                'pries'=>$v->pries,
                'img_name' =>base_url('assets/images/').$v->img_name,
            ];
            $data2[]=$d;
        }
        $data1 = [
            'results' =>$data2,
          
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }

    public function cart_get()
    {

        $token ='';
        $data =[];
        $data2 = [];
        foreach (getallheaders() as $name => $value) {
            if($name =='token'){
                $token = $value;
            } 
        }
        if($token !=''){
            $user = $this->db->get_where("users", ['token' => $token])->row();
             //echo $this->db->last_query();die;
            // print_r($user);die;
            if(!empty($user)){
                $userid =  $user->id;
                $query = "SELECT products.id,products.name,products.description,products.pries,images.img_name,cart.qyt FROM `products` LEFT JOIN images ON images.product_id = products.id LEFT JOIN cart ON cart.pid = products.id WHERE cart.uid = '$userid' AND products.status = '1' GROUP BY products.id ";
                $data =  $this->db->query($query)->result();
               //echo $query;die;
                foreach($data as $v){
                        $string = strip_tags($v->description);
                        if (strlen($string) > 50) {
                            $stringCut = substr($string, 0, 50);
                            $endPoint = strrpos($stringCut, ' ');
                            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                            $string .= '...';
                        }
                    $d = [
                        'id' => $v->id,
                        'name' =>$v->name,
                        'description' =>$string,
                        'pries'=>$v->pries,
                        'qyt' =>$v->qyt,
                        'img_name' =>base_url('assets/images/').$v->img_name,
                    ];
                    $data2[]=$d;
                }
            }
            
        }
        
        $data1 = [
            'results' =>$data2,
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    }

    public function cart_post()
    {

        $data = json_decode(file_get_contents('php://input'), true);
        $id = $data['id'];
        $token ='';
      
        $c ='';
        $data2 = [];
        foreach (getallheaders() as $name => $value) {
            if($name =='token'){
                $token = $value;
            } 
        }
        if($token !=''){
            $user = $this->db->get_where("users", ['token' => $token])->row();
            if(!empty($user)){
                $userid =  $user->id;
                $dm = [
                    'pid' =>$id,
                    'uid' =>$user->id,
                    'qyt' =>1
                ];
                if($this->db->insert("cart", $dm)){
                    $status =1;
                }else{
                    $status =0;
                }
                
            }
            
        }
        
        $data1 = [
            'results' =>'',
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
    
    }
     
    public function callback_post() {        
        $data = json_decode(file_get_contents('php://input'), true);
        $token ='';
        $id = $data['razorpay_order_id'];
        foreach (getallheaders() as $name => $value) {
            if($name =='token'){
                $token = $value;
            } 
        }
        $key_id = RAZOR_KEY_ID;
        $key_secret = RAZOR_KEY_SECRET;
        $api = new Api($key_id, $key_secret);
        $order = $api->order->fetch($id);
       // print_r($order);
        $payments = $api->order->fetch($id)->payments();
        //print_r($payments->items[0]->id);die;
        $merchant_order_id =  rand(10000,99999);
        if($this->insert_oder_items($merchant_order_id,$token)){
            $this->payment_order($payments->items[0],$merchant_order_id);
        }
            
        $data1 = [
            'results' =>'',
            'status' => 1,
            'msg' =>'okkkkkk'
        ];
        $this->response($data1);
  
    } 
    public function insert_oder_items($invoice,$token){
        $user = $this->db->get_where("users", ['token' => $token])->row();
        $userid =  $user->id;
        $sql = "SELECT cart.uid,cart.pid,cart.qyt,products.pries,products.name,products.created_by FROM `cart` LEFT JOIN products ON products.id = cart.pid WHERE cart.uid = '$userid'";
        $products = $this->db->query($sql)->result_array();
        $total = 0;
        foreach ( $products as $items){
           
        $total =  $total +$items['pries'];
           $oitem =[
               'u_id' =>$items['uid'],
               'p_id' =>$items['pid'],
               'qty' =>$items['qyt'],
               'price' =>$items['pries'],
               'product_name' =>$items['name'],
               'total_amt' =>$items['pries'],
               'invoice_no' =>$invoice,
               'paystatus' =>1,
               'seller_id' =>$items['created_by'],
           ];
        
            $insert = $this->db->insert('orders_items', $oitem);
            $product = $this->APImodel->get_by_id('products',$items['pid']);
            $q =  $product->quantity - $items['qyt'];
            
            $userData = array(
                'quantity' => $q,
                );
                //print_r($userData);die;
                $insertUserData = $this->APImodel->update('products',$items['pid'],$userData);
            
        }
        $order = array(
            'u_id'=>$userid,
            'invoice_id'=>$invoice,
            'total_amt'=>  $total,
            'paymet_status' => 'done',
            'date' => date('Y-m-d H:i:s')
        );
     
        if($this->db->insert('orders', $order)){
            $this->db->where('uid', $userid);
            $this->db->delete('cart');
            return true;
        }else{
            return false;
        }
        
    }
    public function payment_order($data,$invoice){
        $data1 = array(
            'ORDERID'=>$invoice,
            'TXNID'=>$data->id,
            'TXNAMOUNT'=>$data->amount/100,
            'PAYMENTMODE'=>$data->method,
            'TXNDATE'=>$data->created_at,
            'STATUS'=>$data->status,
            'email'=>$data->email,
            'contact'=>$data->contact,
            'BANKNAME'=>$data->bank,
            'DATA'=> serialize($data)
        );
        $this->db->insert('payments', $data1);
        return true;
    }

    public function createOrderBasic_get()
    {
        $amount = $_GET['amount'] * 100;
        $key_id = RAZOR_KEY_ID;
        $key_secret = RAZOR_KEY_SECRET;
        $api = new Api($key_id, $key_secret);
        $order = $api->order->create(array(
            'receipt' => '123',
            'amount' =>$amount,
            'currency' => 'INR'
            )
          );
          $data1 = [
                    'id' =>$order->id,
                    'status' => 1,
                    'msg' =>'okkkkkk'
                ];
        $this->response($data1);
    } 	
}