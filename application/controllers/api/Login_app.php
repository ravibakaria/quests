<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
    header('Content-Type: application/json');
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
require APPPATH . 'libraries/REST_Controller.php';

class Login_app extends REST_Controller {

    
    public function __construct() {
       parent::__construct();
       $this->load->model('APImodel');
    }
    public function index_post()
    {   $data = json_decode(file_get_contents('php://input'), true);
        $email = $data['email'];
        $password = $data['password'];
        $var = $this->APImodel->login($email);
        if($var){
            $token = openssl_random_pseudo_bytes(16);
            $token = bin2hex($token);
            $data2 = ['token'=>$token];
            $this->APImodel->set_token($email,$data2);
        }
        
        if($var){
            if($var->password == md5($password)){
                $var1 = $this->APImodel->login($email);
                $data1 = [
                    'token' => $var1->token,
                    'status' => 1,
                    'msg' =>'Login successful'
                ];
                $this->response(['token' => $var1->token], REST_Controller::HTTP_OK);
               
            }else{
                $data1 = [
                    
                    'status' => 0,
                    'msg' =>'Password Worng'
                ];
                $this->response(['error' => 'Password Worng'], REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            $data1 = [
                
                'status' => 0,
                'msg' =>'Email Worng'
            ];
            $this->response(['error' => 'Email Worng'.$email." " .$password ], REST_Controller::HTTP_BAD_REQUEST);
        }
    } 

    public function register_post()
    {
       
        $data = json_decode(file_get_contents('php://input'), true);
        $email = $data['email'];
        $password = $data['password'];
        $name = $data['name'];
        $data = array(
            'email'=>  $email,
            'name'=> $name,
            'password' => md5($password), 
        );
        $user = $this->APImodel->getwhere($email);
        if(empty($user)){
            if($this->APImodel->register($data)){
                $var = $this->APImodel->login($email);
                if($var){
                    $token = openssl_random_pseudo_bytes(16);
                    $token = bin2hex($token);
                    $data2 = ['token'=>$token];
                    $this->APImodel->set_token($email,$data2);
                    $data1 = array(
                        'username'=> $name,
                        "siteUrl" => base_url(),
                    );
                    $subject='Welcome To Quests';
                    
                    $email = $email;
                    $email_params = array();
                    $email_params['to_name'] = $name;
                    $email_params['to_email'] = $email;
                    $email_params['from_email'] = "no-reply@gmail.com";
                    $email_params['subject'] = $subject;
                    $emailotpmessage = $this->load->view('Business_partner/welcomemail',$data1,TRUE);
                    $email_params['message'] = $emailotpmessage;
                    //$this->mail($email_params);
                    $md = md5($password);
                   
                    if($var->password == $md){
                        $var1 = $this->APImodel->login($email);
                        $data1 = [
                            'token' => $var1->token,
                            'status' => 1,
                            'msg' =>'Login successful'
                        ];
                        $this->response(['token' => $var1->token], REST_Controller::HTTP_OK);
                    }else{
                        $data1 = [
                            
                            'status' => 0,
                            'msg' =>'Password Worng'
                        ];
                        $this->response(['error' => 'Password Worng'], REST_Controller::HTTP_BAD_REQUEST);
                    }
                }else{
                    $data1 = [
                        
                        'status' => 0,
                        'msg' =>'Email Worng'
                    ];
                    $this->response(['error' => 'Email Worng'.$email." " .$password ], REST_Controller::HTTP_BAD_REQUEST);
                }
            }else{
                $data1 = [
                        
                    'status' => 0,
                    'msg' =>'Email Worng'
                ];
                $this->response(['error' => 'Email Worng'.$email." " .$password ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            $data1 = [         
                'status' => 0,
                'msg' =>'email alredy teken',
            ];
            $this->response(['error' => 'email alredy teken'.$email." " .$password ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    function mail($params = array()) {
        $this->load->library('phpmailer');
  
       $mailer = $this->phpmailer;
  
       // $mailer
  
       $mailer->IsSMTP(); // telling the class to use SMTP
       $mailer->SMTPDebug = 1;                // enables SMTP debug information (for testing)
       // 1 = errors and messages
       // 2 = messages only
  
       $mailer->SMTPAuth = true;                  // enable SMTP authentication
       $mailer->SMTPSecure = "ssl";                 // sets the prefix to the servier
       $mailer->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
       $mailer->Port = 465;                   // set the SMTP port for the GMAIL server
  
       $mailer->Username = APPLICATION_EMAIL;            //edit
       $mailer->Password = APPLICATION_PASS;
  
  
       $mailer->AltBody = "To view the message, please use an HTML compatible email viewer!";
  
  
       $mailer->FromName = "Noreply";
       $mailer->From = "Noreply@gmail.com";  //edit
       $mailer->isHTML = true;
  
       if (isset($params['from_name'])) {
           $mailer->FromName = $params['from_name'];
       }
  
       if (isset($params['from_email'])) {
           $mailer->From = $params['from_email'];
       }
  
  
       if (!isset($params['subject'])) {
           throw new Exception("Email: Subject is required", 1);
       }
       if (!isset($params['message'])) {
           throw new Exception("Email: Email body is required", 1);
       }
  
       if (!isset($params['to_email']) || (isset($params['to']) && count($params['to']) == 0)) {
           throw new Exception("Email: Atleast one recipient is required", 1);
       }
  
       if (isset($params['to_email'])) {
           $params['to_name'] = (isset($params['to_name'])) ? $params['to_name'] : $params['to_email'];
           $mailer->AddAddress($params['to_email'], $params['to_name']);
       } else {
           throw new Exception("Email: Multiple recipient is not implemented yet", 1);
       }
  
       if (isset($params['addcc']) && !empty($params['addcc'])) {
           foreach ($params['addcc'] as $email => $name)
               $mailer->AddCC($email, $name);
       }
       if (isset($params['addbcc']) && !empty($params['addbcc'])) {
           foreach ($params['addbcc'] as $email => $name)
               $mailer->AddBCC($email, $name);
       }
  
       if (isset($params['addattachment']) && !empty($params['addattachment'])) {
           foreach ($params['addattachment'] as $key => $path)
               $mailer->AddAttachment($path, $key);
       }
  
       $mailer->Subject = $params['subject'];
       $mailer->MsgHTML($params['message']);
  
       $status = 0;
  
       ob_start();
       $mail_send_status = $mailer->Send();
       $mail_send_errors = ob_get_contents();
       ob_end_clean();
      // echo $mail_send_errors;
  
       if ($mail_send_status) {
           $status = 1;
       }else{
           $mailer->ErrorInfo;
           exit;
       }
  
       $mailer->ClearAddresses();
       $mailer->ClearAttachments();
  
       return $status;
   }
}