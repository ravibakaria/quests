<?php

class APImodel extends CI_Model
{
    public $id = 'id'; 
 
    public $order = 'DESC'; 
    function __construct()
    {
          
    }
    public function login($email){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$email);
        $prevQuery = $this->db->get();
        return $prevQuery->row();
    }
    public function register($data){
        $this->db->insert('users',$data);
        
        return true;
    }

    public function set_token($email,$token){
        $this->db->where('email', $email); 
        $this->db->update('users', $token);
       // echo $this->db->last_query();die;
    }
    function update($table,$id, $data) 
    { 
    $this->db->where($this->id, $id); 
    $this->db->update($table, $data); 
    }

    public function  getwhere($email){
        $this->db->where('email', $email); 
        $prevQuery = $this->db->get('users');
        return $prevQuery->row();
    }

    function get_by_id($table,$id) { 
        $this->db->where('id', $id); 
        return $this->db->get($table)->row(); 
    }

    
}